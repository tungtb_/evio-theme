<?php
/**
 * Template for EDD cart element.
 *
 * Template of `core/customize/builder/elements/edd-cart.php`
 */
$show_title    = $atts['show-title'];
$show_total    = $atts['show-total'];
$display_style = $atts['display-style']==''?'style-1':$atts['display-style'];
$cart_icon     = $atts['cart-icon'];
$count_style   = $atts['display-count-position']==''?'top-right':$atts['display-count-position'];

if ($atts['icon-style'] == 'style-2' && $atts['enable-cart-styling'] != '1') {
    $count_style = 'inside';
}

$class   = ['edd-cart'];
$class[] = $atts['icon-style'];
$class[] = $display_style;
$class[] = $count_style;

if (!empty($atts['align'])) {
    $class[] = 'element-align-'.esc_attr($atts['align']);
}

$cart_count = edd_get_cart_quantity();
$cart_total = edd_currency_filter(edd_format_amount(edd_get_cart_total()));

if (empty($cart_count)) {
    $cart_count = '0';
    $class[] = 'cart-empty';
}

if (!(function_exists('edd_is_checkout') && edd_is_checkout())) : ?>
    <div <?php $this->element_class($class);?>>
        <a class="element-cart-link" href="<?php echo esc_url(edd_get_checkout_uri()); ?>" title="<?php echo esc_attr__('View your shopping cart', 'evio') ?>">
            <?php if (!empty($atts['cart_icon'])) { ?>
            <span class="icon-element-cart">
                <i class="<?php echo esc_attr($atts['cart_icon']['icon']) ?>"></i>
                <span class="edd-cart-quantity"><?php echo strval($cart_count); ?></span>
            </span>
            <?php } ?>
            <?php if ($atts['show_title'] || $atts['show_total']) { ?>
                <div class="wrap-right-element-cart">
                    <?php if ($atts['show_title']) { ?>
                        <span class="title-element-cart">
                            <?php esc_html_e('Cart', 'evio'); ?>
                        </span>
                    <?php }
                    if ($atts['show_total']) { ?>
                        <span class="edd-cart-total"><?php echo esc_html($cart_total); ?></span>
                    <?php } ?>
                </div>
            <?php } ?>
        </a>
    </div>
<?php endif; // EOF
