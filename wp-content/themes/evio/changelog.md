# Change Log
---

## Version 3.0 Release Oct 09, 2018


- Theme Options (Customizer ) improved 
- New Header Builder
- New Footer Builder 
- New Mega Menu built-in
- New 1 Click Demo Importer 
- Add Calculate Free Shipping Thresholds
- Add Instant Search AutoComplete
- Add WooCommerce Buy Now button
- Add a lot of Hover on Product Effect 
- Advanced Pagination Options
- Layout based on Bootstrap 4
- Add Catalog Mode 
- Advanced Adaptive Images
- Improve Shop layouts 
- Improve Product Page layouts
- Improve theme style 