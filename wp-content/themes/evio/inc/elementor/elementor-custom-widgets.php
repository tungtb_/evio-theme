<?php 

// Custom settings Icon-Box

add_action( 'elementor/element/icon-box/section_style_content/before_section_end', function( $element, $args ) {

    $element->start_injection(['type' => 'control', 'at' => 'after', 'of' => 'title_color']);

	$element->add_control(
		'title_color_hover',
		[
			'label' => __( 'Color Hover', 'elementor' ),
			'type' => \Elementor\Controls_Manager::COLOR,
			'default' => '',
			'selectors' => [
				'{{WRAPPER}} .elementor-icon-box-content .elementor-icon-box-title:hover, {{WRAPPER}} .elementor-icon-box-content .elementor-icon-box-title a:hover' => 'color: {{VALUE}};',
			],
			'scheme' => [
				'type' => \Elementor\Scheme_Color::get_type(),
				'value' => \Elementor\Scheme_Color::COLOR_1,
			],
		]
	);

    $element->end_injection();

}, 10, 2 );


// Custon Settings Image Box

add_action( 'elementor/element/image-box/section_style_content/before_section_end', function( $element, $args ) {

    $element->start_injection(['type' => 'control', 'at' => 'after', 'of' => 'text_align']);

	$element->add_responsive_control(
		'text_padding',
		[
			'label' => __( 'Padding', 'elementor' ),
			'type' => \Elementor\Controls_Manager::DIMENSIONS,
			'size_units' => [ 'px', 'em', '%' ],
			'selectors' => [
				'{{WRAPPER}} .elementor-image-box-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
			],
			'separator' => 'before',
		]
	);

	$element->add_responsive_control(
		'content_margin',
		[
			'label' => __( 'Margin', 'elementor' ),
			'type' => \Elementor\Controls_Manager::DIMENSIONS,
			'size_units' => [ 'px', 'em', '%' ],
			'selectors' => [
				'{{WRAPPER}} .elementor-image-box-content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
			],
			'separator' => 'before',
		]
	);

	$element->add_control(
		'bgr_content_color',
		[
			'label' => __( 'Background Color', 'elementor' ),
			'type' => \Elementor\Controls_Manager::COLOR,
			'default' => '',
			'selectors' => [
				'{{WRAPPER}} .elementor-image-box-content' => 'background: {{VALUE}};',
			],
			'scheme' => [
				'type' => \Elementor\Scheme_Color::get_type(),
				'value' => \Elementor\Scheme_Color::COLOR_1,
			],
		]
	);

	$element->add_control(
		'bgrhv_content_color',
		[
			'label' => __( 'Background Hover Color', 'elementor' ),
			'type' => \Elementor\Controls_Manager::COLOR,
			'default' => '',
			'selectors' => [
				'{{WRAPPER}} .elementor-image-box-content:hover' => 'background: {{VALUE}};',
			],
			'scheme' => [
				'type' => \Elementor\Scheme_Color::get_type(),
				'value' => \Elementor\Scheme_Color::COLOR_1,
			],
		]
	);

	$element->add_control(
		'content_hover_color',
		[
			'label' => __( 'Content Hover Color', 'elementor' ),
			'type' => \Elementor\Controls_Manager::COLOR,
			'default' => '',
			'selectors' => [
				'{{WRAPPER}} .elementor-image-box-content:hover .elementor-image-box-title,
				 {{WRAPPER}} .elementor-image-box-content:hover .elementor-image-box-description' => 'color: {{VALUE}};',
			],
			'scheme' => [
				'type' => \Elementor\Scheme_Color::get_type(),
				'value' => \Elementor\Scheme_Color::COLOR_1,
			],
		]
	);

    $element->end_injection();

}, 10, 2 );

add_action( 'elementor/element/image-box/section_style_content/before_section_end', function( $element, $args ) {

    $element->start_injection(['type' => 'control', 'at' => 'after', 'of' => 'title_color']);

	$element->add_control(
		'title_hover_color',
		[
			'label' => __( 'Hover Color', 'elementor' ),
			'type' => \Elementor\Controls_Manager::COLOR,
			'default' => '',
			'selectors' => [
				'{{WRAPPER}} .elementor-image-box-content .elementor-image-box-title:hover, .elementor-image-box-content .elementor-image-box-title a:hover' => 'color: {{VALUE}};',
			],
			'scheme' => [
				'type' => \Elementor\Scheme_Color::get_type(),
				'value' => \Elementor\Scheme_Color::COLOR_1,
			],
		]
	);

    $element->end_injection();

}, 10, 2 );


