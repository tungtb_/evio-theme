<?php
/**
 * Meta box for theme
 *
 * @package     Zoo Theme
 * @version     1.0.0
 * @core        3.0.0
 * @author      Zootemplate
 * @link        https://www.zootemplate.com/
 * @copyright   Copyright (c) 2018 ZooTemplate
 */

if ( class_exists( 'RWMB_Loader' ) ):
	add_filter( 'rwmb_meta_boxes', 'zoo_meta_box_options' );
	if ( ! function_exists( 'zoo_meta_box_options' ) ) {
		function zoo_meta_box_options() {
			$zoo_prefix       = "zoo_";
			$zoo_meta_boxes   = array();
			$zoo_meta_boxes[] = array(
				'id'      => $zoo_prefix . 'single_post_heading',
				'title'   => esc_html__( 'Sidebar Config', 'evio' ),
				'pages'   => array( 'post' ),
				'context' => 'side',
				'fields'  => array(
					array(
						'id'      => $zoo_prefix . "blog_single_sidebar_config",
						'type'    => 'select',
						'options' => array(
							'inherit' => esc_html__( 'Inherit', 'evio' ),
							'left'    => esc_html__( 'Left', 'evio' ),
							'right'   => esc_html__( 'Right', 'evio' ),
							'none'    => esc_html__( 'None', 'evio' ),
						),
						'std'     => 'inherit',
						'desc'    => esc_html__( 'Select sidebar layout you want set for this post.', 'evio' )
					),
				)
			);
			//All page
			$zoo_meta_boxes[] = array(
				'id'      => $zoo_prefix . 'layout_single_heading',
				'title'   => esc_html__( 'Layout Single Product', 'evio' ),
				'pages'   => array( 'product' ),
				'context' => 'advanced',
				'fields'  => array(
					array(
						'name'    => esc_html__( 'Layout Options', 'evio' ),
						'id'      => $zoo_prefix . "single_product_layout",
						'type'    => 'select',
						'options' => array(
							'inherit'          => esc_html__( 'Inherit', 'evio' ),
							'vertical-thumb'   => esc_html__( 'Product V1', 'evio' ),
							'horizontal-thumb' => esc_html__( 'Product V2', 'evio' ),
							'carousel'         => esc_html__( 'Product V3', 'evio' ),
							'grid-thumb'       => esc_html__( 'Product V4', 'evio' ),
							'sticky-1'         => esc_html__( 'Product V5', 'evio' ),
							'sticky-2'         => esc_html__( 'Product V6', 'evio' ),
							'sticky-3'         => esc_html__( 'Product V7', 'evio' ),
							//'accordion'        => esc_html__( 'Product V7', 'evio' ),
							'custom'           => esc_html__( 'Custom', 'evio' ),
						),
						'std'     => 'inherit',
					),
					array(
						'name'    => esc_html__( 'Content Options', 'evio' ),
						'id'      => $zoo_prefix . "single_product_content_layout",
						'type'    => 'select',
						'options' => array(
							'inherit'        => esc_html__( 'Inherit', 'evio' ),
							'right_content'  => esc_html__( 'Right Content', 'evio' ),
							'left_content'   => esc_html__( 'Left Content', 'evio' ),
							'full_content'   => esc_html__( 'Full width Content', 'evio' ),
							'sticky_content' => esc_html__( 'Sticky Content', 'evio' ),
						),
						'std'     => 'inherit',
					),
					array(
						'name'    => esc_html__( 'Gallery Options', 'evio' ),
						'id'      => $zoo_prefix . "product_gallery_layout",
						'type'    => 'select',
						'options' => array(
							'inherit'        => esc_html__( 'Inherit', 'evio' ),
							'vertical-left'  => esc_html__( 'Vertical Left Thumb', 'evio' ),
							'vertical-right' => esc_html__( 'Vertical Right Thumb', 'evio' ),
							'horizontal'     => esc_html__( 'Horizontal', 'evio' ),
							'slider'         => esc_html__( 'Slider', 'evio' ),
							'grid'           => esc_html__( 'Grid', 'evio' ),
							'sticky'         => esc_html__( 'Sticky', 'evio' ),
						),
						'std'     => 'inherit',
					),
					array(
						'name'    => esc_html__( 'Gallery Columns', 'evio' ),
						'id'      => $zoo_prefix . "product_gallery_columns",
						'type'    => 'select',
						'options' => array(
							'6' => esc_html__( '6', 'evio' ),
							'5' => esc_html__( '5', 'evio' ),
							'4' => esc_html__( '4', 'evio' ),
							'3' => esc_html__( '3', 'evio' ),
							'2' => esc_html__( '2', 'evio' ),
							'1' => esc_html__( '1', 'evio' ),
							'inherit' => esc_html__( 'Inherit', 'evio' ),
						),
						'std'     => 'inherit',
					),
				)
			);
			$zoo_meta_boxes[] = array(
				'id'      => $zoo_prefix . 'single_product_image_360_heading',
				'title'   => esc_html__( 'Product image 360 view', 'evio' ),
				'pages'   => array( 'product' ),
				'context' => 'advanced',
				'fields'  => array(
					array(
						'id'   => $zoo_prefix . "single_product_image_360",
						'name' => esc_html__( 'Images', 'evio' ),
						'type' => 'image_advanced',
						'desc' => esc_html__( 'Images for 360 degree view.', 'evio' )
					),
				)
			);
			$zoo_meta_boxes[] = array(
				'id'      => $zoo_prefix . 'single_product_video_heading',
				'title'   => esc_html__( 'Product Video', 'evio' ),
				'pages'   => array( 'product' ),
				'context' => 'side',
				'fields'  => array(
					array(
						'id'   => $zoo_prefix . "single_product_video",
						'type' => 'oembed',
						'desc' => esc_html__( 'Enter your embed video url.', 'evio' )
					),
				)
			);
			$zoo_meta_boxes[] = array(
				'id'      => $zoo_prefix . 'single_product_new_heading',
				'title'   => esc_html__( 'Assign product is New', 'evio' ),
				'pages'   => array( 'product' ),
				'context' => 'side',
				'fields'  => array(
					array(
						'id'   => $zoo_prefix . "single_product_new",
						'std'  => '0',
						'type' => 'checkbox',
						'desc' => esc_html__( 'Is New Product.', 'evio' )
					),
				)
			);
			$zoo_meta_boxes[] = array(
				'id'      => 'title_meta_box',
				'title'   => esc_html__( 'Layout Options', 'evio' ),
				'pages'   => array( 'page', 'post' ),
				'context' => 'advanced',
				'fields'  => array(
					array(
						'name' => esc_html__( 'Title & Breadcrumbs Options', 'evio' ),
						'desc' => esc_html__( '', 'evio' ),
						'id'   => $zoo_prefix . "heading_title",
						'type' => 'heading'
					),
					array(
						'name' => esc_html__( 'Disable Title', 'evio' ),
						'desc' => esc_html__( '', 'evio' ),
						'id'   => $zoo_prefix . "disable_title",
						'std'  => '0',
						'type' => 'checkbox'
					),
					array(
						'name' => esc_html__( 'Disable Breadcrumbs', 'evio' ),
						'desc' => esc_html__( '', 'evio' ),
						'id'   => $zoo_prefix . "disable_breadcrumbs",
						'std'  => '0',
						'type' => 'checkbox'
					),
					array(
						'name' => esc_html__( 'Page Layout', 'evio' ),
						'desc' => esc_html__( '', 'evio' ),
						'id'   => $zoo_prefix . "body_heading",
						'type' => 'heading'
					),
					array(
						'name'    => esc_html__( 'Layout Options', 'evio' ),
						'id'      => $zoo_prefix . "site_layout",
						'type'    => 'select',
						'options' => array(
							'inherit'    => esc_html__( 'Inherit', 'evio' ),
							'normal'     => esc_html__( 'Normal', 'evio' ),
							'boxed'      => esc_html__( 'Boxed', 'evio' ),
							'full-width' => esc_html__( 'Full Width', 'evio' ),
						),
						'std'     => 'inherit',
					),
					array(
						'name' => esc_html__( 'Page Max Width', 'evio' ),
						'desc' => esc_html__( 'Accept only number. If not set, it will follow customize config.', 'evio' ),
						'id'   => $zoo_prefix . "site_max_width",
						'type' => 'number'
					),
				)
			);

			return $zoo_meta_boxes;
		}
	}
endif;