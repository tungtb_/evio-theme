<?php
/**
 * Customize for Shop loop product
 */
return [
    [
        'type' => 'section',
        'name' => 'zoo_single_product',
        'title' => esc_html__('Product Page', 'evio'),
        'panel' => 'woocommerce',
        'theme_supports' => 'woocommerce'
    ],
    [
        'name' => 'zoo_single_product_general_settings',
        'type' => 'heading',
        'label' => esc_html__('General Settings', 'evio'),
        'section' => 'zoo_single_product',
        'theme_supports' => 'woocommerce'
    ],
    [
        'name' => 'zoo_enable_product_share',
        'type' => 'checkbox',
        'section' => 'zoo_single_product',
        'label' => esc_html__('Enable Product Share', 'evio'),
        'checkbox_label' => esc_html__('Show product share if checked.', 'evio'),
        'theme_supports' => 'woocommerce',
        'default' => 1
    ],
    [
        'name' => 'zoo_enable_sticky_add_to_cart',
        'type' => 'checkbox',
        'section' => 'zoo_single_product',
        'label' => esc_html__('Enable Sticky Add to Cart', 'evio'),
        'checkbox_label' => esc_html__('Block sticky add to cart will show if checked.', 'evio'),
        'theme_supports' => 'woocommerce',
        'default' => 1,
    ],[
        'name' => 'zoo_enable_buy_now',
        'type' => 'checkbox',
        'section' => 'zoo_single_product',
        'label' => esc_html__('Enable Buy Now', 'evio'),
        'checkbox_label' => esc_html__('Buy now button allow use auto redirect to cart page after product added to cart.', 'evio'),
        'theme_supports' => 'woocommerce',
        'default' => 0,
    ],[
        'name' => 'zoo_enable_term_buy_now',
        'type' => 'checkbox',
        'section' => 'zoo_single_product',
        'label' => esc_html__('Enable Term Buy Now', 'evio'),
        'checkbox_label' => esc_html__('Label Agree with term with show.', 'evio'),
        'theme_supports' => 'woocommerce',
        'default' => 1,
        'required' => ['zoo_enable_buy_now', '==', '1'],
    ],[
        'name' => 'zoo_enable_single_product_nav',
        'type' => 'checkbox',
        'section' => 'zoo_single_product',
        'label' => esc_html__('Enable Next & Previous product', 'evio'),
        'theme_supports' => 'woocommerce',
        'default' => 0,
        'checkbox_label' => esc_html__('Show next and previous products navigation.', 'evio'),

    ],
    [
        'name' => 'zoo_single_product_layout_settings',
        'type' => 'heading',
        'label' => esc_html__('Layout & Gallery Settings', 'evio'),
        'section' => 'zoo_single_product',
        'theme_supports' => 'woocommerce'
    ],
    [
        'name' => 'zoo_single_product_layout',
        'type' => 'select',
        'section' => 'zoo_single_product',
        'title' => esc_html__('Layout', 'evio'),
        'default' => 'vertical-thumb',
        'choices' => [
            'vertical-thumb' => esc_html__('Product V1', 'evio'),
            'horizontal-thumb' => esc_html__('Product V2', 'evio'),
            'carousel' => esc_html__('Product V3', 'evio'),
            'grid-thumb' => esc_html__('Product V4', 'evio'),
            'sticky-1' => esc_html__('Product V5', 'evio'),
            'sticky-2' => esc_html__('Product V6', 'evio'),
            'sticky-3' => esc_html__('Product V7', 'evio'),
            //'accordion' => esc_html__('Accordion', 'evio'),
            'custom' => esc_html__('Custom', 'evio'),
        ]
    ],[
        'name' => 'zoo_single_product_content_layout',
        'type' => 'select',
        'section' => 'zoo_single_product',
        'title' => esc_html__('Content Layout', 'evio'),
        'default' => 'right_content',
        'required' => ['zoo_single_product_layout', '==', 'custom'],
        'choices' => [
            'right_content' => esc_html__('Right Content', 'evio'),
            'left_content' => esc_html__('Left Content', 'evio'),
            'full_content' => esc_html__('Full width Content', 'evio'),
            'sticky_content' => esc_html__('Sticky Content', 'evio'),
        ]
    ],[
        'name' => 'zoo_product_gallery_layout',
        'type' => 'select',
        'section' => 'zoo_single_product',
        'title' => esc_html__('Gallery Type', 'evio'),
        'default' => 'vertical-left',
        'required' => ['zoo_single_product_layout', '==', 'custom'],
        'choices' => [
            'vertical-left' => esc_html__('Vertical Left Thumb', 'evio'),
            'vertical-right' => esc_html__('Vertical Right Thumb', 'evio'),
            'horizontal' => esc_html__('Horizontal', 'evio'),
            'slider' => esc_html__('Slider', 'evio'),
            'grid' => esc_html__('Grid', 'evio'),
        ]
    ],
    [
        'name' => 'zoo_product_thumb_cols',
        'type' => 'number',
        'label' => esc_html__('Thumbnail columns', 'evio'),
        'description' => esc_html__('Number thumbnail gallery display same time.', 'evio'),
        'section' => 'zoo_single_product',
        'input_attrs' => array(
            'min' => 2,
            'max' => 6,
            'class'=>'zoo-range-slider'
        ),
        'default' => 4,
    ],
    [
        'name' => 'zoo_product_tabs_setting',
        'type' => 'select',
        'section' => 'zoo_single_product',
        'title' => esc_html__('Tabs Type', 'evio'),
        'default' => 'tabs',
        'required' => ['zoo_single_product_layout', '==', 'custom'],
        'choices' => [
            'tabs' => esc_html__('Tabs', 'evio'),
            'accordion' => esc_html__('Accordion', 'evio'),
        ]
    ],
    [
        'name' => 'zoo_enable_product_zoom',
        'type' => 'checkbox',
        'section' => 'zoo_single_product',
        'label' => esc_html__('Enable Product Gallery Zoom', 'evio'),
        'theme_supports' => 'woocommerce',
        'checkbox_label' => esc_html__('Enable product zoom for product gallery.', 'evio'),
        'default' => 1,
    ],[
        'name' => 'zoo_enable_product_lb',
        'type' => 'checkbox',
        'section' => 'zoo_single_product',
        'label' => esc_html__('Enable Product Gallery Light Box', 'evio'),
        'theme_supports' => 'woocommerce',
        'checkbox_label' => esc_html__('Enable light box for product gallery.', 'evio'),
        'default' => 1,
    ],
    [
        'name' => 'zoo_single_product_cart_extend_settings',
        'type' => 'heading',
        'label' => esc_html__('Cart Extend Feature', 'evio'),
        'section' => 'zoo_single_product',
        'theme_supports' => 'woocommerce'
    ],
    [
        'name' => 'zoo_single_product_cart_size_guide',
        'type' => 'select',
        'section' => 'zoo_single_product',
        'title' => esc_html__('Size Guide', 'evio'),
        'description' => esc_html__('Disable by set None. Label display in product page will apply follow page title.', 'evio'),
        'default' => '0',
        'choices' => zoo_get_pages()
    ],[
        'name' => 'zoo_single_product_cart_delivery',
        'type' => 'select',
        'section' => 'zoo_single_product',
        'title' => esc_html__('Delivery & Return', 'evio'),
        'description' => esc_html__('Disable by set None. Label display in product page will apply follow page title.', 'evio'),
        'default' => '0',
        'choices' => zoo_get_pages()
    ],[
        'name' => 'zoo_single_product_cart_ask_product',
        'type' => 'select',
        'section' => 'zoo_single_product',
        'title' => esc_html__('Ask about this product', 'evio'),
        'description' => esc_html__('Disable by set None. Label display in product page will apply follow page title.', 'evio'),
        'default' => '0',
        'choices' => zoo_get_pages()
    ],
    [
        'name' => 'zoo_single_product_extend_settings',
        'type' => 'heading',
        'label' => esc_html__('Extend Feature', 'evio'),
        'section' => 'zoo_single_product',
        'theme_supports' => 'woocommerce'
    ],
    [
        'name' => 'zoo_product_enable_sold_per_day',
        'type' => 'checkbox',
        'section' => 'zoo_single_product',
        'label' => esc_html__('Enable Show sold on this day', 'evio'),
        'theme_supports' => 'woocommerce',
        'checkbox_label' => esc_html__('Show number products sold inside 24h.', 'evio'),
        'default' => 1,
    ],
    [
        'name' => 'zoo_product_sold_fake_data',
        'type' => 'number',
        'label' => esc_html__('Minimum sold', 'evio'),
        'description' => esc_html__('Fake sold current product, auto disable if set 0.', 'evio'),
        'section' => 'zoo_single_product',
        'default' => 10,
    ],[
        'name' => 'zoo_product_sold_fake_max_data',
        'type' => 'number',
        'label' => esc_html__('Maximum sold', 'evio'),
        'section' => 'zoo_single_product',
        'default' => 50,
    ],
    [
        'name' => 'zoo_product_stock_countdown',
        'type' => 'checkbox',
        'section' => 'zoo_single_product',
        'label' => esc_html__('Enable Stock Countdown', 'evio'),
        'theme_supports' => 'woocommerce',
        'checkbox_label' => esc_html__('Show Stock Countdown of deal product.', 'evio'),
        'default' => 1,
    ],
    [
        'name' => 'zoo_product_get_order',
        'type' => 'checkbox',
        'section' => 'zoo_single_product',
        'label' => esc_html__('Enable Get Order time', 'evio'),
        'theme_supports' => 'woocommerce',
        'checkbox_label' => esc_html__('Show expected day will deliver.', 'evio'),
        'default' => 1,
    ],
    [
        'name' => 'zoo_product_get_order_day',
        'type' => 'number',
        'label' => esc_html__('Order Day', 'evio'),
        'description' => esc_html__('Expected day will get order.', 'evio'),
        'section' => 'zoo_single_product',
        'default' => 3,
    ],
    [
        'name' => 'zoo_product_get_visitor',
        'type' => 'checkbox',
        'section' => 'zoo_single_product',
        'label' => esc_html__('Enable Show Visitor product', 'evio'),
        'theme_supports' => 'woocommerce',
        'checkbox_label' => esc_html__('Show visitor on product.', 'evio'),
        'default' => 1,
    ],
    [
        'name' => 'zoo_product_visitor_fake_data',
        'type' => 'number',
        'label' => esc_html__('Minimum visitor', 'evio'),
        'description' => esc_html__('Fake visitor current product, auto disable if current visitor more than this value .', 'evio'),
        'section' => 'zoo_single_product',
        'default' => 100,
    ],[
        'name' => 'zoo_product_visitor_fake_max_data',
        'type' => 'number',
        'label' => esc_html__('Maximum visitor', 'evio'),
        'section' => 'zoo_single_product',
        'default' => 200,
    ],
    [
        'name' => 'zoo_product_free_shipping_notice',
        'type' => 'checkbox',
        'section' => 'zoo_single_product',
        'label' => esc_html__('Enable Free shipping amount notice', 'evio'),
        'theme_supports' => 'woocommerce',
        'checkbox_label' => esc_html__('Show Free shipping amount in product summary.', 'evio'),
        'default' => 1,
    ],
    [
        'name' => 'zoo_product_guarantee_safe_checkout',
        'type' => 'image',
        'section' => 'zoo_single_product',
        'label' => esc_html__('Guarantee Safe Checkout Logo', 'evio'),
        'theme_supports' => 'woocommerce',
    ],
    [
        'name' => 'zoo_single_product_upsell_settings',
        'type' => 'heading',
        'label' => esc_html__('Upsell Product', 'evio'),
        'section' => 'zoo_single_product',
        'theme_supports' => 'woocommerce'
    ],
    [
        'name' => 'zoo_upsell_products_layout',
        'type' => 'select',
        'section' => 'zoo_single_product',
        'title' => esc_html__('Layout', 'evio'),
        'default' => 'grid',
        'choices' => [
            'grid' => esc_html__('Grid', 'evio'),
            'carousel' => esc_html__('Carousel', 'evio'),
        ]
    ],
    [
        'name' => 'zoo_upsell_products_cols',
        'type' => 'number',
        'label' => esc_html__('Columns', 'evio'),
        'description' => esc_html__('Number products per row.', 'evio'),
        'section' => 'zoo_single_product',
        'input_attrs' => array(
            'min' => 1,
            'max' => 10,
            'class'=>'zoo-range-slider'
        ),
        'default' => 4,
    ],
    [
        'name' => 'zoo_single_product_recent_settings',
        'type' => 'heading',
        'label' => esc_html__('Recently Viewed Product', 'evio'),
        'section' => 'zoo_single_product',
        'theme_supports' => 'woocommerce'
    ],
    [
        'type' => 'checkbox',
        'name' => 'zoo_enable_recently_viewed_product',
        'label' => esc_html__('Enable Recently Viewed Products', 'evio'),
        'section' => 'zoo_single_product',
        'default' => '1',
        'checkbox_label' => esc_html__('Recently Viewed Products will show if enabled.', 'evio'),
    ],
    [
        'name' => 'zoo_recently_viewed_product_number',
        'type' => 'number',
        'label' => esc_html__('Maximum recently viewed product', 'evio'),
        'description' => esc_html__('Total number recently viewed product.', 'evio'),
        'section' => 'zoo_single_product',
        'required' => ['zoo_enable_recently_viewed_product', '==', '1'],
        'input_attrs' => array(
            'min' => 1,
            'max' => 20,
            'class'=>'zoo-range-slider'
        ),
        'default' => 8,
    ],

    [
        'name' => 'zoo_recently_viewed_product_layout',
        'type' => 'select',
        'section' => 'zoo_single_product',
        'title' => esc_html__('Layout', 'evio'),
        'default' => 'grid',
        'required' => ['zoo_enable_recently_viewed_product', '==', '1'],
        'choices' => [
            'grid' => esc_html__('Grid', 'evio'),
            'carousel' => esc_html__('Carousel', 'evio'),
        ]
    ],
    [
        'name' => 'zoo_recently_viewed_product_cols',
        'type' => 'number',
        'label' => esc_html__('Columns', 'evio'),
        'description' => esc_html__('Number products per row.', 'evio'),
        'section' => 'zoo_single_product',
        'required' => ['zoo_enable_recently_viewed_product', '==', '1'],
        'input_attrs' => array(
            'min' => 1,
            'max' => 10,
            'class'=>'zoo-range-slider'
        ),
        'default' => 4,
    ],    [
        'name' => 'zoo_single_product_related_settings',
        'type' => 'heading',
        'label' => esc_html__('Related Product', 'evio'),
        'section' => 'zoo_single_product',
        'theme_supports' => 'woocommerce'
    ],
    [
        'type' => 'checkbox',
        'name' => 'zoo_enable_related_products',
        'label' => esc_html__('Enable Related Products', 'evio'),
        'section' => 'zoo_single_product',
        'default' => '1',
        'checkbox_label' => esc_html__('Related Products will show if enabled.', 'evio'),
    ],
    [
        'name' => 'zoo_related_products_number',
        'type' => 'number',
        'label' => esc_html__('Maximum related product', 'evio'),
        'description' => esc_html__('Total number related product.', 'evio'),
        'section' => 'zoo_single_product',
        'required' => ['zoo_enable_related_products', '==', '1'],
        'input_attrs' => array(
            'min' => 1,
            'max' =>20,
            'class'=>'zoo-range-slider'
        ),
        'default' => 8,
    ],

    [
        'name' => 'zoo_related_products_layout',
        'type' => 'select',
        'section' => 'zoo_single_product',
        'title' => esc_html__('Layout', 'evio'),
        'default' => 'grid',
        'required' => ['zoo_enable_related_products', '==', '1'],
        'choices' => [
            'grid' => esc_html__('Grid', 'evio'),
            'carousel' => esc_html__('Carousel', 'evio'),
        ]
    ],
    [
        'name' => 'zoo_related_products_cols',
        'type' => 'number',
        'label' => esc_html__('Columns', 'evio'),
        'description' => esc_html__('Number products per row.', 'evio'),
        'section' => 'zoo_single_product',
        'required' => ['zoo_enable_related_products', '==', '1'],
        'input_attrs' => array(
            'min' => 1,
            'max' => 10,
            'class'=>'zoo-range-slider'
        ),
        'default' => 4,
    ],
];