<?php
/**
 * Customize for General Style
 */
return [
    [
        'name' => 'zoo_style',
        'type' => 'panel',
        'label' => esc_html__('Style', 'evio'),
    ], [
        'name' => 'zoo_general_style',
        'type' => 'section',
        'label' => esc_html__('General Style', 'evio'),
        'panel' => 'zoo_style',
        'description' => esc_html__('Leave option blank if you want use default style of theme.', 'evio'),
    ],
    [
        'name' => 'zoo_general_style_heading_color',
        'type' => 'heading',
        'section' => 'zoo_general_style',
        'title' => esc_html__('Color', 'evio'),
    ],
    [
        'name' => 'zoo_color_preset',
        'type' => 'select',
        'section' => 'zoo_general_style',
        'title' => esc_html__('Preset', 'evio'),
        'description' => esc_html__('Predefined color scheme to Style', 'evio'),
        'default' => 'default',
        'choices' => [
            'default' => esc_html__('Default', 'evio'),
            'black' => esc_html__('Black', 'evio'),
            'blue' => esc_html__('Blue', 'evio'),
            'red' => esc_html__('Red', 'evio'),
            'yellow' => esc_html__('Yellow', 'evio'),
            'green' => esc_html__('Green', 'evio'),
            'grey' => esc_html__('Grey', 'evio'),
            'custom' => esc_html__('Custom', 'evio'),
        ]
    ],
    [
        'name' => 'zoo_primary_color',
        'type' => 'color',
        'section' => 'zoo_general_style',
        'title' => esc_html__('Primary color', 'evio'),
        'selector' => ".accent-color",
        'css_format' => 'color: {{value}};',
        'description' => esc_html__('Primary color of theme apply only when preset is custom.', 'evio'),
        'required'=>['zoo_color_preset','==','custom']
    ],[
        'name' => 'zoo_site_color',
        'type' => 'color',
        'section' => 'zoo_general_style',
        'title' => esc_html__('Text color', 'evio'),
        'selector' => "body",
        'css_format' => 'color: {{value}};',
    ],[
        'name' => 'zoo_site_link_color',
        'type' => 'color',
        'section' => 'zoo_general_style',
        'title' => esc_html__('Link color', 'evio'),
        'selector' => "a",
        'css_format' => 'color: {{value}};',
    ],[
        'name' => 'zoo_site_link_color_hover',
        'type' => 'color',
        'section' => 'zoo_general_style',
        'title' => esc_html__('Link color hover', 'evio'),
        'selector' => "a:hover",
        'css_format' => 'color: {{value}};',
    ],[
        'name' => 'zoo_site_heading_color',
        'type' => 'color',
        'section' => 'zoo_general_style',
        'title' => esc_html__('Heading color', 'evio'),
        'selector' => "h1, h2, h3, h4, h5, h6",
        'css_format' => 'color: {{value}};',
    ],
    [
        'name' => 'zoo_general_heading_bg',
        'type' => 'heading',
        'section' => 'zoo_general_style',
        'title' => esc_html__('Background', 'evio'),
    ],
    [
        'name' => 'zoo_general_bg',
        'type' => 'styling',
        'section' => 'zoo_general_style',
        'title'  => esc_html__('Background', 'evio'),
        'selector' => [
            'normal' => "body",
        ],
        'field_class'=>'no-hide no-heading',
        'css_format' => 'styling', // styling
        'fields' => [
            'normal_fields' => [
                'text_color' => false,
                'link_color' => false,
                'link_hover_color' => false,
                'padding' => false,
                'box_shadow' => false,
                'border_radius' => false,
                'border_style' => false,
                'border_heading' => false,
                'bg_heading' => false,
                'margin' => false
            ],
            'hover_fields' => false
        ]
    ],
];
