<?php
/**
 * Customize for Shop loop product
 */
return [
	[
		'type'           => 'section',
		'name'           => 'zoo_shop',
		'title'          => esc_html__( 'Shop Page', 'evio' ),
		'panel'          => 'woocommerce',
		'theme_supports' => 'woocommerce'
	],
	[
		'name'           => 'zoo_shop_general_settings',
		'type'           => 'heading',
		'label'          => esc_html__( 'General Settings', 'evio' ),
		'section'        => 'zoo_shop',
		'theme_supports' => 'woocommerce'
	],
	[
		'type'        => 'number',
		'name'        => 'zoo_products_number_items',
		'label'       => esc_html__( 'Product Per Page', 'evio' ),
		'section'     => 'zoo_shop',
		'description' => esc_html__( 'Number product display per page.', 'evio' ),
		'input_attrs' => array(
			'min'   => 0,
			'max'   => 100,
			'class' => 'zoo-range-slider'
		),
		'default'     => 9
	],
	[
		'name'           => 'zoo_enable_catalog_mod',
		'type'           => 'checkbox',
		'section'        => 'zoo_shop',
		'label'          => esc_html__( 'Enable Catalog Mod', 'evio' ),
		'checkbox_label' => esc_html__( 'Will be enabled if checked.', 'evio' ),
		'theme_supports' => 'woocommerce',
		'default'        => 0
	],
	[
		'name'           => 'zoo_enable_free_shipping_notice',
		'type'           => 'checkbox',
		'section'        => 'zoo_shop',
		'label'          => esc_html__( 'Enable Free Shipping Notice', 'evio' ),
		'checkbox_label' => esc_html__( 'Free shipping thresholds will show in cart if checked.', 'evio' ),
		'theme_supports' => 'woocommerce',
		'default'        => 1,
	],
	[
		'name'           => 'zoo_enable_shop_heading',
		'type'           => 'checkbox',
		'section'        => 'zoo_shop',
		'label'          => esc_html__( 'Enable Shop Heading', 'evio' ),
		'checkbox_label' => esc_html__( 'Display product archive title and description.', 'evio' ),
		'theme_supports' => 'woocommerce',
		'default'        => 1,
	],
	[
		'name'        => 'zoo_shop_banner',
		'type'        => 'image',
		'section'     => 'zoo_shop',
		'title'       => esc_html__( 'Shop banner', 'evio' ),
		'description' => esc_html__( 'Banner image display at top Products page. It will override by Category image.', 'evio' ),
		'required'    => [ 'zoo_enable_shop_heading', '==', '1' ],
	],
	[
		'name'           => 'zoo_shop_layout_settings',
		'type'           => 'heading',
		'label'          => esc_html__( 'Layout Settings', 'evio' ),
		'section'        => 'zoo_shop',
		'theme_supports' => 'woocommerce'
	],
	[
		'name'    => 'zoo_shop_sidebar',
		'type'    => 'select',
		'section' => 'zoo_shop',
		'title'   => esc_html__( 'Shop Sidebar', 'evio' ),
		'default' => 'left',
		'choices' => [
			'top'        => esc_html__( 'Top (Horizontal)', 'evio' ),
			'left'       => esc_html__( 'Left', 'evio' ),
			'right'      => esc_html__( 'Right', 'evio' ),
			'off-canvas' => esc_html__( 'Off canvas', 'evio' ),
		]
	],
	[
		'type'           => 'checkbox',
		'name'           => 'zoo_shop_full_width',
		'label'          => esc_html__( 'Enable Shop Full Width', 'evio' ),
		'section'        => 'zoo_shop',
		'default'        => '0',
		'checkbox_label' => esc_html__( 'Shop layout will full width if enabled.', 'evio' ),
	],
	[
		'type'           => 'checkbox',
		'name'           => 'zoo_enable_shop_loop_item_border',
		'label'          => esc_html__( 'Enable Product border', 'evio' ),
		'section'        => 'zoo_shop',
		'default'        => '0',
		'checkbox_label' => esc_html__( 'Enable border for product item.', 'evio' ),
	],
	[
		'type'           => 'checkbox',
		'name'           => 'zoo_enable_highlight_featured_product',
		'label'          => esc_html__( 'Enable High light Featured Product', 'evio' ),
		'section'        => 'zoo_shop',
		'default'        => '0',
		'checkbox_label' => esc_html__( 'Featured product will display bigger more than another product.', 'evio' ),
	],
	[
		'type'        => 'number',
		'name'        => 'zoo_shop_loop_item_gutter',
		'label'       => esc_html__( 'Product Gutter', 'evio' ),
		'section'     => 'zoo_shop',
		'description' => esc_html__( 'White space between product item.', 'evio' ),
		'input_attrs' => array(
			'min'   => 0,
			'max'   => 100,
			'class' => 'zoo-range-slider'
		),
		'default'     => 30
	],
	[
		'name'        => 'zoo_shop_cols_desktop',
		'type'        => 'number',
		'label'       => esc_html__( 'Shop loop columns', 'evio' ),
		'description' => esc_html__( 'Number product per row in shop page.', 'evio' ),
		'section'     => 'zoo_shop',
		'input_attrs' => array(
			'min'   => 2,
			'max'   => 6,
			'class' => 'zoo-range-slider'
		),
		'default'     => 4
	],
	[
		'name'        => 'zoo_shop_cols_tablet',
		'type'        => 'number',
		'label'       => esc_html__( 'Shop loop columns on Tablet', 'evio' ),
		'section'     => 'zoo_shop',
		'unit'        => false,
		'input_attrs' => array(
			'min'   => 1,
			'max'   => 4,
			'class' => 'zoo-range-slider'
		),
		'default'     => 2,
	],
	[
		'name'        => 'zoo_shop_cols_mobile',
		'type'        => 'number',
		'label'       => esc_html__( 'Shop loop columns on Mobile', 'evio' ),
		'section'     => 'zoo_shop',
		'input_attrs' => array(
			'min'   => 1,
			'max'   => 2,
			'class' => 'zoo-range-slider'
		),
		'default'     => 2,
	],
	[
		'name'           => 'zoo_shop_product_item_settings',
		'type'           => 'heading',
		'label'          => esc_html__( 'Product Item Settings', 'evio' ),
		'section'        => 'zoo_shop',
		'theme_supports' => 'woocommerce'
	],
	[
		'name'           => 'zoo_enable_shop_loop_cart',
		'type'           => 'checkbox',
		'section'        => 'zoo_shop',
		'label'          => esc_html__( 'Enable Shop Loop Cart', 'evio' ),
		'checkbox_label' => esc_html__( 'Button Add to cart will show if checked.', 'evio' ),
		'theme_supports' => 'woocommerce',
		'default'        => 0,
		'required'       => [ 'zoo_enable_catalog_mod', '!=', 1 ],
	],
	[
		'name'    => 'zoo_shop_cart_icon',
		'type'    => 'icon',
		'section' => 'zoo_shop',
		'title'   => esc_html__( 'Cart icon', 'evio' ),
		'default' => [
			'type' => 'zoo-icon',
			'icon' => 'zoo-icon-cart'
		]
	],
	[
		'type'           => 'checkbox',
		'name'           => 'zoo_enable_alternative_images',
		'label'          => esc_html__( 'Enable Alternative Image', 'evio' ),
		'section'        => 'zoo_shop',
		'default'        => '1',
		'checkbox_label' => esc_html__( 'Alternative Image will show if checked.', 'evio' ),
	],
	[
		'type'    => 'select',
		'name'    => 'zoo_sale_type',
		'label'   => esc_html__( 'Sale label type display', 'evio' ),
		'section' => 'zoo_shop',
		'default' => 'text',
		'choices' => [
			'numeric' => esc_html__( 'Numeric', 'evio' ),
			'text'    => esc_html__( 'Text', 'evio' ),
		]
	],
	[
		'type'           => 'checkbox',
		'name'           => 'zoo_enable_shop_new_label',
		'label'          => esc_html__( 'Show New Label', 'evio' ),
		'section'        => 'zoo_shop',
		'default'        => '1',
		'checkbox_label' => esc_html__( 'Stock New will show if checked.', 'evio' ),
	],
	[
		'type'           => 'checkbox',
		'name'           => 'zoo_enable_shop_stock_label',
		'label'          => esc_html__( 'Show Stock Label', 'evio' ),
		'section'        => 'zoo_shop',
		'default'        => '1',
		'checkbox_label' => esc_html__( 'Stock label will show if checked.', 'evio' ),
	],
	[
		'type'           => 'checkbox',
		'name'           => 'zoo_enable_quick_view',
		'label'          => esc_html__( 'Enable Quick View', 'evio' ),
		'section'        => 'zoo_shop',
		'default'        => '1',
		'checkbox_label' => esc_html__( 'Button quick view will show if checked.', 'evio' ),
	],
	[
		'type'           => 'checkbox',
		'name'           => 'zoo_enable_shop_loop_rating',
		'label'          => esc_html__( 'Show rating', 'evio' ),
		'section'        => 'zoo_shop',
		'default'        => '1',
		'checkbox_label' => esc_html__( 'Show rating in product item if checked.', 'evio' ),
	],
	/*Product image thumb for gallery*/
	[
		'name'           => 'zoo_gallery_thumbnail_heading',
		'type'           => 'heading',
		'label'          => esc_html__( 'Gallery Thumbnail', 'evio' ),
		'section'        => 'woocommerce_product_images',
		'theme_supports' => 'woocommerce'
	],
	[
		'type'           => 'number',
		'name'           => 'zoo_gallery_thumbnail_width',
		'label'          => esc_html__( 'Gallery Thumbnail Width', 'evio' ),
		'section'        => 'woocommerce_product_images',
		'default'        => '120',
		'description' => esc_html__( 'Max width of image for gallery thumbnail.', 'evio' ),
	],
	[
		'type'           => 'number',
		'name'           => 'zoo_gallery_thumbnail_height',
		'label'          => esc_html__( 'Gallery Thumbnail Height', 'evio' ),
		'section'        => 'woocommerce_product_images',
		'default'        => '120',
	],
	[
		'type'           => 'checkbox',
		'name'           => 'zoo_gallery_thumbnail_crop',
		'label'          => esc_html__( 'Crop', 'evio' ),
		'section'        => 'woocommerce_product_images',
		'default'        => '0',
		'checkbox_label' => esc_html__( 'Crop Gallery Thumbnail.', 'evio' ),
	],
];
