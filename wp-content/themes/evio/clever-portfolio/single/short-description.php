<?php
/**
 * The template displaying content single portfolio format Gallery with layout List.
 *
 * @package      clever-portfolio\Templates
 * @version      1.0.0
 * @author       Zootemplate
 * @link         http://www.zootemplate.com
 * @copyright    Copyright (c) 2016 Zootemplate
 * @license      GPL v2
 * @since        clever-portfolio 1.0
 */

$clever_settings=clever_portfolio_get_settings();
$clever_meta=clever_portfolio_single_meta(); 

$sort_content =  $clever_meta['short_description'];

?>

<div class="portfolio-desc">
	<?php if( $clever_meta['short_description'] != '' ) : ?>
		<?php echo esc_html($sort_content); ?> 
	<?php endif; ?> 
</div>