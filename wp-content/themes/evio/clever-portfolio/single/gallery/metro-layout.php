<?php
/**
 * The template displaying content single portfolio format Gallery  with layout Metro.
 *
 * @package      clever-portfolio\Templates
 * @version      1.0.0
 * @author       Zootemplate
 * @link         http://www.zootemplate.com
 * @copyright    Copyright (c) 2016 Zootemplate
 * @license      GPL v2
 * @since        clever-portfolio 1.0
 */
$clever_meta=clever_portfolio_single_meta();
$clever_lightbox = '';
$clever_settings = clever_portfolio_get_settings();
if (isset($clever_settings["single_enable_lightbox"])) {
    if ($clever_settings["single_enable_lightbox"] == 1) {
        $clever_lightbox = 'clever-lightbox-gallery';
    }
}
?>
<div class="cp-wrap-content">
    <div class="wrapper-portfolio-info">
        <?php
            the_title('<h1 class="title-portfolio">', '</h1>');
        ?>
        <?php clever_get_template_part('clever-portfolio', 'single/', 'infor', true); ?>
        <?php clever_get_template_part('clever-portfolio', 'single/', 'short-description', true); ?>
    </div>
    <div class="cp-gallery">
        <ul class="cp-wrap-imgs <?php echo esc_attr($clever_lightbox)?>" data-col="<?php echo esc_attr($clever_meta['columns']);?>" data-width="<?php echo esc_attr($clever_settings['single_metro_width']);?>">
            <?php 
            if (count($clever_meta['galleries']) > 0) {
                foreach ($clever_meta['galleries'] as $img) {
                    $item=wp_get_attachment_image_src($img,'full');
                    if($item) {
                        $img_url = $item[0];
                        $width = $item[1];
                        $height = $item[2];
                        $img_title = get_the_title($img);
                        ?>
                        <li class="portfolio-img">
                            <a href="<?php echo esc_url($img_url) ?>" title="<?php echo esc_attr($img_title); ?>">
                                <img src="<?php echo esc_attr($img_url) ?>" height="<?php echo esc_attr($height) ?>"
                                     width="<?php echo esc_attr($width) ?>" alt="<?php echo esc_attr($img_title); ?>"/>
                            </a>
                        </li>
                        <?php
                    }
                }
            }
            ?>
        </ul>
    </div>
    <div class="cp-content">
        <?php
        the_content();
        ?>
    </div>
</div>

<?php

clever_get_template_part('clever-portfolio', 'single/', 'pagination', true);
//Js load
wp_enqueue_script('isotope');
wp_enqueue_script('imagesloaded');
