<?php
/**
 * The template displaying content single portfolio format Gallery with layout List.
 *
 * @package      clever-portfolio\Templates
 * @version      1.0.0
 * @author       Zootemplate
 * @link         http://www.zootemplate.com
 * @copyright    Copyright (c) 2016 Zootemplate
 * @license      GPL v2
 * @since        clever-portfolio 1.0
 */
?>

<div class="cp-wrap-content tung tung">
    <div class="wrapper-thumbnail">
        <?php if( has_post_thumbnail() ){
            the_post_thumbnail('full');
        } ?>
    </div>
    <div class="wrapper-portfolio-info">
        <?php
           the_title('<h1 class="title-portfolio">', '</h1>');
        ?>
        <?php clever_get_template_part('clever-portfolio', 'single/', 'infor', true); ?>
        <?php clever_get_template_part('clever-portfolio', 'single/', 'short-description.php', true); ?>
    </div>
    <?php clever_get_template_part('clever-portfolio', 'single/gallery/', 'gallery', true); ?>
    <div class="cp-content">
        <?php
        the_content();
        ?>
    </div>
</div>

<?php clever_get_template_part('clever-portfolio', 'single/', 'pagination', true); ?>