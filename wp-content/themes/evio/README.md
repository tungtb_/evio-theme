# Zoo Theme Core Framework

## 1. Requirements

    1. PHP >= 5.3.2
    1. MySQL >= 5.5
    2. WordPress >= 4.3

## 2. Installation

Clone this repos, alter or modified anything outside the `lib` folder to meet your theme features and functionality.

### 2.1  Must-have folders and files.

### 2.1.0 `lib`

**:exclamation:DO NOT TOUCH ANYTHING INSIDE THIS FOLDER!**

Contain core features and functionality, everything inside this folder is well maintained and updated frequently. It means that any modification will be lost in a single commit.

**What're core features and functionality?**

#### Core features

1. [title-tag](https://codex.wordpress.org/Title_Tag):

    ```php
    add_theme_support( 'title-tag' );
    ```

2. [post-thumbnails](https://codex.wordpress.org/Post_Thumbnails):

    ```php
    add_theme_support( 'post-thumbnails' );
    ```

3. [post-formats](https://codex.wordpress.org/Post_Formats):

    ```php
    add_theme_support( 'post-formats', array( 'aside', 'gallery', 'image', 'quote', 'video', 'audio' ) );
    ```

4. [html5](https://codex.wordpress.org/Theme_Markup):

    ```php
    add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );    
    ```

5. [custom-logo](https://codex.wordpress.org/Theme_Logo):

    ```php
    add_theme_support( 'custom-logo', apply_filters('zoo_custom_logo_feature_args', array(
        'height'      => 100,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    ) ) );    
    ```

6. [automatic-feed-links](https://codex.wordpress.org/Automatic_Feed_Links):

    ```php
    add_theme_support( 'automatic-feed-links' );
    ```

7. [woocommerce](https://docs.woocommerce.com/document/declare-woocommerce-support-in-third-party-theme/):

    ```php
    add_theme_support( 'woocommerce' );
    ```

8. [customize-selective-refresh-widgets](https://make.wordpress.org/core/2016/03/22/implementing-selective-refresh-support-for-widgets/):

    ```php
    add_theme_support( 'customize-selective-refresh-widgets' );
    ```

9. [nav menus](https://codex.wordpress.org/Navigation_Menus) - there's one pre-registered menu:

    ```php
    register_nav_menus( array(
        'primary' => esc_html__( 'Primary Menu', 'evio' ),
    ) );
    ```
10. [sidebars](https://codex.wordpress.org/Sidebars) -  there're two pre-registered sidebars:

    ```php
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'evio' ),
        'id'            => 'sidebar',
        'description'   => esc_html__( 'Add widgets here to display it on Sidebar 1.', 'evio' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar 2', 'evio' ),
        'id'            => 'sidebar-2',
        'description'   => esc_html__( 'Add widgets here to display it on Sidebar 2.', 'evio' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
    ```

**YOU SHOULD NOT RE-REGISTER THOSE FEATURES!**

#### Hooks

1. [zoo_before_customize_register](https://github.com/cleversoft/clever-theme/blob/dev/lib/customizer/class-zoo-customizer.php#L111):

You may use this hook to add extra panels, sections, fields, settings into Zoo Theme Core customizer.

Example

    /**
     * Register custom theme mods from a theme
     *
     * @param  object  $zoo_customizer  \Zoo_Customizer
     * @param  object  $wp_customize    \WP_Customize_Manager
     * @param  mixed   $mods            Theme mods - value of `get_theme_mods()`
     */
    function register_theme_mods($zoo_customizer, $wp_customize, $mods)
    {
      $zoo_customizer->add_section( 'custom', array(
          'title' => esc_html__( 'Custom CSS/JS', 'evio' )
      ) );
      $zoo_customizer->add_field( 'zoo_customizer', array(
          'type'     => 'textarea',
          'settings' => 'zoo_custom_css',
          'label'    => esc_html__( 'Custom CSS', 'evio' ),
          'section'  => 'custom',
          'default'  => ''
      ) );
      $zoo_customizer->add_field( 'zoo_customizer', array(
          'type'     => 'textarea',
          'settings' => 'zoo_custom_js',
          'label'    => esc_html__( 'Custom JS', 'evio' ),
          'section'  => 'custom',
          'default'  => ''
      ) );
    }
    add_action('zoo_before_customize_register', 'register_theme_mods', 10, 3);

### 2.1.1 `inc/plugins`

This folder contains all pre-packaged plugins zip files which will be installed while installing theme via theme setup wizard.

**:pizza: How it works?**

Zoo Theme Core uses [TGM Plugin Activation](http://tgmpluginactivation.com) to include and install theme required plugins. So, you know how it work, right?

1. Put all pre-packaged plugins' zip files into the `inc/plugins` folder.

2. Register recommended and required plugins with TGMPA:

Example:

    function zoo_register_required_plugins()
    {
        $plugins = array(
            array(
                'name'      => 'WPBakery Visual Composer',
                'slug'      => 'js_composer',
                'required'  => true,
                'source'    => 'js_composer.zip',
                'version'   => '5.0.1'
            ),
            array(
                'name'     => 'Contact Form 7',
                'slug'     => 'contact-form-7',
                'required' => true,
                'version'  => '4.6'
            )
        );

        $config = array(
            'default_path' => ZOO_THEME_DIR . 'inc/plugins/',
        );

        tgmpa($plugins, $config);
    }
    add_action( 'tgmpa_register', 'zoo_register_required_plugins', 10, 0 );

That's all! There're four pre-registered plugins: WPBakery Visual Composer, Contact Form 7, Revolution Slider and Clever Visual Composer Addon. You should not re-register those plugins.

### 2.1.2 `inc/sample-data`

This folder contains all starter content for theme. All sample content inside this folder will be imported automatically while installing theme via theme setup wizard.

**:pizza: How it works?**

1. `base` - contain base content files:

    - `content.xml` - base starter content.
    - 'customizer.dat' - base theme customize options.
    - `slider.zip` - base revolution slider.
    - `widgets.wie` - base widgets.

2.  `default` - default homepage content which will be installed by default if user doesn't select a custom homepage.

3. Others - contain starter content of other homepages.

**::exclamation: ZOO THEME CORE USES FILES' NAME TO IDENTIFY WHICH FILE SHOULD BE IMPORT SO YOU SHOULD NOT CHANGE FILES' NAME**

### 2.1.3 `inc/init.php`

Since the `functions.php` will be used as bootstrap file of theme core, theme must load extra features and functionality from the `inc/init.php` file. `functions.php` will load the `inc/init.php` to initialize your theme.

**:exclamation: YOU MUST NOT MODIFY FUNCTIONS.PHP OR DELETE INIT.PHP FILE!**

### 3 Fix issue sample data can't import after update WooCommerce to 3.6.x
**:exclamation: Apply for site demo have been install WooCommerce 3.6.x when waiting solution from WC**
- Download [this file](https://github.com/woocommerce/woocommerce/blob/d2d342f30eec99b606e81b02ef678ba2e7737cc0/includes/admin/class-wc-admin.php)
- Use FTP and go this folder on demo site: 'site_name/plugins/woocommerce/includes/admin/'
- Override file `class-wc-admin.php` by file downloaded before.
- Export Sample data like normal.