<?php
/**
 * Evio Custom widgets.
 * Custom widget control for widget of cafe-lite plugins.
 */
// use Elementor\Controls_Stack;

require ZOO_THEME_DIR .'cafe/widgets/cafe-icon-widget.php';
require ZOO_THEME_DIR .'cafe/widgets/cafe-team-member-widget.php';
require ZOO_THEME_DIR .'cafe/widgets/cafe-post-widget.php';
require ZOO_THEME_DIR .'cafe/widgets/cafe-banner-widget.php';
require ZOO_THEME_DIR .'cafe/widgets/cafe-button-widget.php';


