<?php 

// Adding Content Field

add_action( 'elementor/element/clever-icon/icon_settings/before_section_end', function( $element, $args ) {

    $element->start_injection(['type' => 'control', 'at' => 'after', 'of' => 'title']);

    $element->add_control('content', [
            'label' => esc_html__('Content', 'cafe-lite'),
            'type' => \Elementor\Controls_Manager::TEXTAREA,
    ]);

    $element->end_injection();

}, 10, 2 );

// Remove Box-shadow Icon Settings

add_action( 'elementor/element/before_section_end', function($section, $section_id, $args) {
        if( $section->get_name() == 'clever-icon' && $section_id == 'normal_style_settings' ) 
        {
            $section->remove_control('icon_shadow_box_shadow_type');
        }
}, 10, 3 );

// Box-shadow Icon Settings

add_action( 'elementor/element/clever-icon/normal_style_settings/before_section_end', function( $element, $args ) {

    $element->start_injection(['type' => 'control', 'at' => 'after', 'of' => 'icon_bg']);

    $element->add_group_control(
        \Elementor\Group_Control_Box_Shadow::get_type(),
        [
            'name' => 'icon_shadow_ct',
            'separator' => 'before',
            'selector' => '{{WRAPPER}} .cafe-icon:before',
        ]
    );

    $element->end_injection();

}, 10, 2 );

// Animation Icon Settings

add_action( 'elementor/element/clever-icon/hover_style_settings/before_section_end', function( $element, $args ) {

    $element->start_injection(['type' => 'control', 'at' => 'before', 'of' => 'color_hover']);

    $element->add_control(
		'using_animation',
		[
			'label' => __( 'Using animation', 'cafe-lite' ),
			'type' => \Elementor\Controls_Manager::SWITCHER,
			'label_on' => __( 'Yes', 'cafe-lite' ),
			'label_off' => __( 'No', 'cafe-lite' ),
			'return_value' => 'yes',
			'default' => 'no',
		]
    );

    $element->end_injection();

}, 10, 2 );

// Title Settings 

add_action( 'elementor/element/clever-icon/title_style_settings/before_section_end', function( $element, $args ) {

    $element->start_injection(['type' => 'control', 'at' => 'after', 'of' => 'title_padding']);

    $element->add_responsive_control('title_margin', [
	    'label' => esc_html__('Margin', 'cafe-lite'),
	    'type' => \Elementor\Controls_Manager::DIMENSIONS,
	    'size_units' => ['px', '%', 'em'],
	    'selectors' => [
		    '{{WRAPPER}} .cafe-icon-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
	    ],
    ]);

    $element->end_injection();

}, 10, 2 );

// Content Settings 

add_action( 'elementor/element/clever-icon/title_style_settings/after_section_end', function( $element, $args ) {

	$element->start_controls_section(
		'content_style_settings',
		[
			'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			'label' => __( 'Content', 'cafe-lite' ),
		]
	);

	$element->add_control('content_color', [
	    'label' => esc_html__('Content Color', 'cafe-lite'),
	    'type' => \Elementor\Controls_Manager::COLOR,
	    'selectors' => [
		    '{{WRAPPER}} .box-content' => 'color: {{VALUE}};'
	    ]
    ]);

    $element->add_group_control(
	    \Elementor\Group_Control_Typography::get_type(),
	    [
		    'name' => 'content_typography',
		    'selector' => '{{WRAPPER}} .box-content',
		    'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
	    ]
    );
    $element->add_responsive_control('content_padding', [
	    'label' => esc_html__('Padding', 'cafe-lite'),
	    'type' => \Elementor\Controls_Manager::DIMENSIONS,
	    'size_units' => ['px', '%', 'em'],
	    'selectors' => [
		    '{{WRAPPER}} .box-content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
	    ],
    ]);

    $element->add_responsive_control('content_margin', [
	    'label' => esc_html__('Margin', 'cafe-lite'),
	    'type' => \Elementor\Controls_Manager::DIMENSIONS,
	    'size_units' => ['px', '%', 'em'],
	    'selectors' => [
		    '{{WRAPPER}} .box-content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
	    ],
    ]);

	$element->end_controls_section();

}, 10, 2 );