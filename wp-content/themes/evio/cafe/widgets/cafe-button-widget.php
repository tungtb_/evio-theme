<?php 

// Remove Button Algin

add_action( 'elementor/element/before_section_end', function($section, $section_id, $args) {
    if( $section->get_name() == 'clever-button' && $section_id == 'normal_style_settings' ) 
    {
        $section->remove_control('text_align');
    }
}, 10, 3 );

// Remove Border Width

add_action( 'elementor/element/before_section_end', function($section, $section_id, $args) {
    if( $section->get_name() == 'clever-button' && $section_id == 'normal_style_settings' ) 
    {
        $section->remove_control('button_border_width');
    }
}, 10, 3 );

// Remove button border Color

add_action( 'elementor/element/before_section_end', function($section, $section_id, $args) {
    if( $section->get_name() == 'clever-button' && $section_id == 'normal_style_settings' ) 
    {
        $section->remove_control('button_border');
    }
}, 10, 3 );

// Add New Button Align

add_action( 'elementor/element/clever-button/normal_style_settings/before_section_end', function( $element, $args ) {

    $element->start_injection(['type' => 'control', 'at' => 'after', 'of' => 'button_style']);

        $element->add_responsive_control(
            'bt_text_align',
            [
                'label' => __( 'Alignment', 'elementor' ),
                'type' => \Elementor\Controls_Manager::CHOOSE,
                'options' => [
                    'left'    => [
                        'title' => __( 'Left', 'elementor' ),
                        'icon' => 'eicon-text-align-left',
                    ],
                    'center' => [
                        'title' => __( 'Center', 'elementor' ),
                        'icon' => 'eicon-text-align-center',
                    ],
                    'right' => [
                        'title' => __( 'Right', 'elementor' ),
                        'icon' => 'eicon-text-align-right',
                    ],
                    'justify' => [
                        'title' => __( 'Justified', 'elementor' ),
                        'icon' => 'eicon-text-align-justify',
                    ],
                ],
                'prefix_class' => 'elementor%s-align-',
                'default' => '',
            ]
        );


    $element->end_injection();

}, 10, 2 );

// Add new Border Button

add_action( 'elementor/element/clever-button/normal_style_settings/before_section_end', function( $element, $args ) {

    $element->start_injection(['type' => 'control', 'at' => 'before', 'of' => 'border_radius']);

        $element->add_group_control(
            \Elementor\Group_Control_Border::get_type(),
            [
                'name' => 'clever_button_border',
                'selector' => '{{WRAPPER}} .cafe-button:before',
            ]
        );
    
    $element->end_injection();

}, 10, 2 );