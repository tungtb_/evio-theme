<?php 

// Adding Style List 

add_action( 'elementor/element/clever-posts/layout_settings/before_section_end', function( $element, $args ) {

    $element->start_injection(['type' => 'control', 'at' => 'after', 'of' => 'col']);

    $element->add_control('style_list', [
        'label' => esc_html__('Style', 'cafe-lite'),
        'type' =>  \Elementor\Controls_Manager::SELECT,
        'default' => 'style_1',
        'options' => [
            'style_1' => esc_html__('Style 1', 'cafe-lite'),
            'style_2' => esc_html__('Style 2', 'cafe-lite'),
        ],
        'condition' => [
            'layout' =>  ['list']
        ],
    ]);

    $element->end_injection();

}, 10, 2 );

// Items Settings 

add_action( 'elementor/element/clever-posts/blog_item_style/before_section_end', function( $element, $args ) {

    $element->start_injection(['type' => 'control', 'at' => 'after', 'of' => 'content_padding']);

    $element->add_control('align_content', [
        'label' => esc_html__('Alignment', 'cafe-lite'),
        'type' => \Elementor\Controls_Manager::CHOOSE,
        'options' => [
            'left'    => [
                'title' => esc_html__('Left', 'elementor' ),
                'icon' => 'fa fa-align-left',
            ],
            'center' => [
                'title' => esc_html__('Center', 'elementor' ),
                'icon' => 'fa fa-align-center',
            ],
            'right' => [
                'title' => esc_html__('Right', 'elementor' ),
                'icon' => 'fa fa-align-right',
            ],
        ],
        //'default' => 'center',
        'selectors' => [
            '{{WRAPPER}} .cafe-posts .cafe-post-item, .cafe-posts .cafe-post-item .entry-content:not(.excerpt)' => 'text-align: {{VALUE}};',
        ],
    ]);


    $element->end_injection();

}, 10, 2 );


// Pagination Settings

add_action( 'elementor/element/clever-posts/readmore_style/after_section_end', function( $element, $args ) {

    $element->start_controls_section(
        'pagination_settings',
        [
            'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            'label' => __( 'Pagination', 'cafe-lite' ),
        ]
    );

    $element->add_control('pagination_color', [
        'label' => esc_html__('Color', 'cafe-lite'),
        'type' => \Elementor\Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .cafe-pagination .cafe_pagination-item' => 'color: {{VALUE}};'
        ]
    ]);

    $element->add_control('pagination_color_hover', [
        'label' => esc_html__('Color Hover', 'cafe-lite'),
        'type' => \Elementor\Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .cafe-pagination .cafe_pagination-item' => 'color: {{VALUE}};'
        ]
    ]);

    $element->add_control('pagination_bgr_cl', [
        'label' => esc_html__('Background Color', 'cafe-lite'),
        'type' => \Elementor\Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .cafe-pagination .cafe_pagination-item' => 'color: {{VALUE}};'
        ]
    ]);

    $element->add_control('pagination_bgr_cl_hv', [
        'label' => esc_html__('Background Color Hover', 'cafe-lite'),
        'type' => \Elementor\Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .cafe-pagination .cafe_pagination-item' => 'color: {{VALUE}};'
        ]
    ]);

    $element->add_responsive_control('pagination_padding', [
        'label' => esc_html__('Padding', 'cafe-lite'),
        'type' => \Elementor\Controls_Manager::DIMENSIONS,
        'size_units' => ['px', '%', 'em'],
        'selectors' => [
            '{{WRAPPER}} .cafe-pagination .cafe_pagination-item' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
    ]);

    $element->add_responsive_control('pagination_margin', [
        'label' => esc_html__('Margin', 'cafe-lite'),
        'type' => \Elementor\Controls_Manager::DIMENSIONS,
        'size_units' => ['px', '%', 'em'],
        'selectors' => [
            '{{WRAPPER}} .cafe-pagination' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
        ],
    ]);

    $element->add_control('align', [
        'label' => esc_html__('Alignment', 'cafe-lite'),
        'type' => \Elementor\Controls_Manager::CHOOSE,
        'options' => [
            'left'    => [
                'title' => esc_html__('Left', 'elementor' ),
                'icon' => 'fa fa-align-left',
            ],
            'center' => [
                'title' => esc_html__('Center', 'elementor' ),
                'icon' => 'fa fa-align-center',
            ],
            'right' => [
                'title' => esc_html__('Right', 'elementor' ),
                'icon' => 'fa fa-align-right',
            ],
        ],
        //'default' => 'center',
        'selectors' => [
            '{{WRAPPER}} .cafe-pagination' => 'text-align: {{VALUE}};',
        ],
    ]);

    $element->end_controls_section();

}, 10, 2 );