<?php


// Remove Member Style

add_action( 'elementor/element/before_section_end', function($section, $section_id, $args) {
        if( $section->get_name() == 'clever-team-member' && $section_id == 'settings' ) 
        {
            $section->remove_control('style');
        }
}, 10, 3 );

// Adding Member Style

add_action( 'elementor/element/clever-team-member/settings/before_section_end', function( $element, $args ) {

    $element->start_injection(['type' => 'control', 'at' => 'after', 'of' => 'member_bio']);

    $element->add_control('style_new', [
        'label'         => esc_html__('Layout Style News', 'cafe-lite'),
        'type'          => \Elementor\Controls_Manager::SELECT,
        'default'       => 'style-5',
        'options' => [
			'style-1' => esc_html__('Style 1', 'cafe-lite' ),
			'style-2' => esc_html__('Style 2', 'cafe-lite' ),
			'style-3' => esc_html__('Style 3', 'cafe-lite' ),
			'style-4' => esc_html__('Style 4', 'cafe-lite' ),
			'style-5' => esc_html__('Style 5', 'cafe-lite' ),
        ],
    ]);

    $element->end_injection();

}, 10, 2 );


add_action( 'elementor/element/clever-team-member/styling_member_social_block/before_section_end', function( $element, $args ) {

    $element->start_injection(['type' => 'control', 'at' => 'after', 'of' => 'bg_member_social_hover']);

    $element->add_group_control(
        \Elementor\Group_Control_Border::get_type(),
        [
            'name' => 'icmb_border',
            'label' => esc_html__('Border', 'cafe-lite'),
            'placeholder' => '1px',
            'default' => '1px',
            'selector' => '{{WRAPPER}} .cafe-team-member .cafe-member-social-item a',
            'separator' => 'before',
        ]
    );

    $element->add_control('icmb_border_color_hover', [
        'label' => esc_html__('Color Hover', 'cafe-lite'),
        'type' => \Elementor\Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .cafe-team-member .cafe-member-social-item a:hover' => 'border-color: {{VALUE}};'
        ]
    ]);

    $element->add_responsive_control('icon_mb_border_radius', [
	    'label' => esc_html__('Border Radius', 'cafe-lite'),
	    'type' => \Elementor\Controls_Manager::DIMENSIONS,
	    'size_units' => ['px', '%'],
	    'selectors' => [
		    '{{WRAPPER}} .cafe-team-member .cafe-member-social-item a' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
	    ],
    ]);

    $element->end_injection();

}, 10, 2 );
