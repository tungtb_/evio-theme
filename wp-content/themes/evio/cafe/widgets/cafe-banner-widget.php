<?php

// Adding Settings Subtitle 

add_action( 'elementor/element/clever-banner/content_settings/before_section_end', function( $element, $args ) {

    $element->start_injection(['type' => 'control', 'at' => 'before', 'of' => 'title']);

    $element->add_control('sub_title', [
        'label' => esc_html__('Sub Title', 'cafe-lite'),
        'type' =>  \Elementor\Controls_Manager::TEXTAREA,
        'default' => esc_html__('Sub Title', 'cafe-lite')
    ]);

    $element->end_injection();

}, 10, 2 );

// Adding Settings Style Subtitle

add_action( 'elementor/element/clever-banner/normal_style_settings/before_section_end', function( $element, $args ) {

    $element->start_injection(['type' => 'control', 'at' => 'before', 'of' => 'title_color_heading']);

    $element->add_control('sub_title_heading', [
        'label' => esc_html__('Sub Title', 'cafe-lite'),
        'type' => \Elementor\Controls_Manager::HEADING
    ]);
    $element->add_control('sub_title_color', [
        'label' => esc_html__('Sub Title Color', 'cafe-lite'),
        'type' => \Elementor\Controls_Manager::COLOR,
        'selectors' => [
            '{{WRAPPER}} .cafe-banner .cafe-wrap-content .sub-title p' => 'color: {{VALUE}};'
        ]
    ]);
    $element->add_group_control(
        \Elementor\Group_Control_Typography::get_type(),
        [
            'name' => 'sub_title_typography',
            'selector' => '{{WRAPPER}} .cafe-banner .cafe-wrap-content .sub-title p',
            'scheme' => \Elementor\Scheme_Typography::TYPOGRAPHY_1,
        ]
    );
    $element->add_control( 'sub_title_space', [
	    'label'     => esc_html__('Space', 'cafe-lite' ),
	    'type'      => \Elementor\Controls_Manager::SLIDER,
	    'range'     => [
		    'px' => [
			    'min' => 0,
			    'max' => 100,
		    ],
	    ],
	    'selectors' => [
		    '{{WRAPPER}} .cafe-banner .cafe-wrap-content .sub-title p' => 'margin-bottom: {{SIZE}}{{UNIT}};',
	    ],
    ] );
    $element->add_control(
        'sub_title_divider',
        [
            'type' => \Elementor\Controls_Manager::DIVIDER,
            'style' => 'thick',
        ]
    );


    $element->end_injection();

}, 10, 2 );