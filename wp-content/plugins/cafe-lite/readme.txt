=== Clever Addons for Elementor ===
Contributors: zootemplate
License: GPLv2+
License URI: http://www.gnu.org/licenses/gpl.html
Tags: addons, elementor, elementor widgets, elementor elements, elementor templates
Requires at least: 4.7
Tested up to: 5.2
Stable tag: 1.0.0


== Description ==

[Clever Addons for Elementor](https://cleveraddon.com/clever-addon-for-elementor/) provides high quality widgets and templates which are easy to use and customize. Help you create stunning websites quickly.

Works best on [Hello Theme](https://wordpress.org/themes/hello-elementor/) from [Elementor.com](https://elementor.com/).

See [Demo](http://cleveraddon4elementor.wp1.zootemplate.com/) and [online documentation](https://doc.cleveraddon.com/clever-mega-menu-elementor) for more info.


###Key Features:

####Ready To Use

With pre-made templates and widgets, you can quickly build anything beautifully.

####Easy To Customize

Supercharge your page with more than 100+ widget options and page settings.

####Light Weight

Resources are optimized for super fast loading and instant live editing.

###Free Widgets:

* Clever Portfolios
* Clever Single Scroll To
* Clever Image Comparison
* Clever Time Line
* Clever Multi Banner
* Clever Banner
* Clever Video Light Box
* Clever Services
* Clever Posts
* Clever Count Down
* Clever Testimonial
* Clever Icon
* Clever Product Advanced
* Clever Team Member
* Clever Instagram
* Clever Scroll To
* Clever Contact Form 7
* Clever Slider
* Clever Revolution Slider
* Clever Button
* Clever Single Product
* Clever Edd Tabs

###Pro Widgets:

* Clever Product Grid Tabs
* Clever Product Categories Nav
* Clever Hide This
* Clever Progress Bar
* Clever Product Deal
* Clever Product Category Banner
* Clever Product Carousel With Category Tabs
* Clever Product With Banner And Tabs
* Clever Product List Tags
* Clever Image 360 View
* Clever Product Carousel
* Clever Pricing Table
* Clever Product Deal And Asset Tabs
* Clever Product List Categories
* Clever Auto Typing

####Help & Support
If you have any issue, feel free to get help at [Plugin Support](https://wordpress.org/support/plugin/cafe-lite/). We really appreciate your feedbacks.

= Further Reading =
For more info, please check out [plugin documentation](https://doc.cleveraddon.com/clever-addon-for-elementor/).

