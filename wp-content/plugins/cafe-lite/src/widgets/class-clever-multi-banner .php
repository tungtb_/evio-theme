<?php namespace Cafe\Widgets;

use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Background;

/**
 * CleverMultiBanner
 *
 * @author CleverSoft <hello.cleversoft@gmail.com>
 * @package CAFE
 */
final class CleverMultiBanner extends CleverWidgetBase
{
    /**
     * @return string
     */
    function get_name()
    {
        return 'clever-multi-banner';
    }

    /**
     * @return string
     */
    function get_title()
    {
        return esc_html__('Clever Multi Banner', 'cafe-lite');
    }

    /**
     * @return string
     */
    function get_icon()
    {
        return 'cs-font clever-icon-news-grid';
    }

    /**
     * Register controls
     */
    protected function _register_controls()
    {
        

        $repeater = new \Elementor\Repeater();

        $repeater->add_control('image', [
            'label' => esc_html__('Upload Image', 'cafe-lite'),
            'description' => esc_html__('Select an image for the banner.', 'cafe-lite'),
            'type' => Controls_Manager::MEDIA,
            'dynamic' => ['active' => true],
            'show_external' => true,
            'default' => [
                'url' => CAFE_URI . '/assets/img/banner-placeholder.png'
            ]
        ]);
        $repeater->add_control('link', [
            'label' => esc_html__('Link', 'cafe-lite'),
            'type' => Controls_Manager::URL,
            'description' => esc_html__('Redirect link when click to banner.', 'cafe-lite'),
        ]);
        $repeater->add_control('title', [
            'label' => esc_html__('Title', 'cafe-lite'),
            'placeholder' => esc_html__('What is the title of this banner.', 'cafe-lite'),
            'description' => esc_html__('What is the title of this banner.', 'cafe-lite'),
            'type' => Controls_Manager::TEXT,
            'dynamic' => ['active' => true],
            'default' => esc_html__('Clever Multi Banner', 'cafe-lite'),
            'label_block' => false
        ]);
        $repeater->add_control('title_tag',[
            'label' => esc_html__('HTML Tag', 'cafe-lite'),
            'description' => esc_html__('Select a heading tag for the title. Headings are defined with H1 to H6 tags.', 'cafe-lite'),
            'type' => Controls_Manager::SELECT,
            'default' => 'h3',
            'options' => [
                'h1' => 'H1',
                'h2' => 'H2',
                'h3' => 'H3',
                'h4' => 'H4',
                'h5' => 'H5',
                'h6' => 'H6',
                'p'  => 'P'
            ],
            'label_block' => true,
        ]);
        $repeater->add_control('des', [
            'label' => esc_html__('Description', 'cafe-lite'),
            'description' => esc_html__('Give a description to this banner.', 'cafe-lite'),
            'type' => Controls_Manager::WYSIWYG,
            'dynamic' => ['active' => true],
            'default' => esc_html__('A web banner or banner ad is a form of advertising. It is intended to attract traffic to a website by linking to the website of the advertiser. - Wikipedia', 'cafe-lite'),
            'label_block' => true
        ]);
        $repeater->add_control('button_label', [
            'label' => esc_html__('Button Label', 'cafe-lite'),
            'placeholder' => esc_html__('Button', 'cafe-lite'),
            'description' => esc_html__('Leave it blank if don\'t use it', 'cafe-lite'),
            'type' => Controls_Manager::TEXT,
            'dynamic' => ['active' => true],
            'default' => esc_html__('Button', 'cafe-lite'),
            'label_block' => false
        ]);
        $repeater->add_control('button_icon', [
            'label' => esc_html__('Icon', 'cafe-lite'),
            'type' => 'clevericon'
        ]);
        $repeater->add_control('button_icon_pos', [
            'label' => esc_html__('Icon position', 'cafe-lite'),
            'type' => Controls_Manager::SELECT,
            'default' => 'after',
            'options' => [
                'before' => esc_html__('Before', 'cafe-lite'),
                'after' => esc_html__('After', 'cafe-lite'),
            ]
        ]);
        $repeater->add_control('button_style', [
            'label' => esc_html__('Button style', 'cafe-lite'),
            'type' => Controls_Manager::SELECT,
            'default' => 'normal',
            'options' => [
                'normal' => esc_html__('Normal', 'cafe-lite'),
                'underline' => esc_html__('Underline', 'cafe-lite'),
                'outline' => esc_html__('Outline', 'cafe-lite'),
            ]
        ]);

        $this->start_controls_section('clever_banner_image__settings', [
            'label' => esc_html__('Multi Image', 'cafe-lite')
        ]);

        $this->add_control(
            'repeater',
            [
                'label' => esc_html__('Repeater List', 'plugin-domain' ),
                'type' => \Elementor\Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_setting', [
                'label' => esc_html__('Setting', 'cafe-lite')
            ]);

        $this->add_control('layout', [
            'label'         => esc_html__('Layout', 'cafe-lite'),
            'description'   => esc_html__('', 'cafe-lite'),
            'type'          => Controls_Manager::SELECT,
            'default'       => 'carousel',
            'options' => [
                'carousel' => esc_html__('Carousel', 'cafe-lite' ),
                'grid'  => esc_html__('Grid', 'cafe-lite' ),
            ],
        ]);
            // Grid
        $this->add_responsive_control('columns',[
            'label'         => esc_html__('Columns for row', 'cafe-lite' ),
            'type'          => Controls_Manager::SLIDER,
            'range' => [
                'col' =>[
                    'min' => 1,
                    'max' => 6,
                ]
            ],
            'devices' => [ 'desktop', 'tablet', 'mobile' ],
            'desktop_default' => [
                'size' => 4,
                'unit' => 'col',
            ],
            'tablet_default' => [
                'size' => 3,
                'unit' => 'col',
            ],
            'mobile_default' => [
                'size' => 2,
                'unit' => 'col',
            ],
            'condition'     => [
                'layout' => 'grid',
            ],
            
        ]);
            // Carousel
        $this->add_responsive_control('slides_to_show',[
            'label'         => esc_html__('Slides to Show', 'elementor' ),
            'type'          => Controls_Manager::SLIDER,
            'range' => [
                'px' =>[
                    'min' => 1,
                    'max' => 10,
                ]
            ],
            'devices' => [ 'desktop', 'tablet', 'mobile' ],
            'desktop_default' => [
                'size' => 4,
                'unit' => 'px',
            ],
            'tablet_default' => [
                'size' => 3,
                'unit' => 'px',
            ],
            'mobile_default' => [
                'size' => 2,
                'unit' => 'px',
            ],
            'condition'     => [
                'layout' => 'carousel',
            ],
            
        ]);

        $this->add_control('speed', [
            'label'         => esc_html__('Carousel: Speed to Scroll', 'cafe-lite'),
            'description'   => esc_html__('', 'cafe-lite'),
            'type'          => Controls_Manager::NUMBER,
            'default'       => 5000,
            'condition'     => [
                'layout' => 'carousel',
            ],
            
        ]);
        $this->add_control('scroll', [
            'label'         => esc_html__('Carousel: Slide to Scroll', 'cafe-lite'),
            'description'   => esc_html__('', 'cafe-lite'),
            'type'          => Controls_Manager::NUMBER,
            'default'       => 1,
            'condition'     => [
                'layout' => 'carousel',
            ],
        ]);
        $this->add_responsive_control('autoplay', [
            'label'         => esc_html__('Carousel: Auto Play', 'cafe-lite'),
            'description'   => esc_html__('', 'cafe-lite'),
            'type'          => Controls_Manager::SWITCHER,
            'label_on' => esc_html__('Show', 'cafe-lite' ),
            'label_off' => esc_html__('Hide', 'cafe-lite' ),
            'return_value' => 'true',
            'default' => 'true',
            'condition'     => [
                'layout' => 'carousel',
            ],
        ]);
        $this->add_responsive_control('show_pag', [
            'label'         => esc_html__('Carousel: Pagination', 'cafe-lite'),
            'description'   => esc_html__('', 'cafe-lite'),
            'type'          => Controls_Manager::SWITCHER,
            'label_on' => esc_html__('Show', 'cafe-lite' ),
            'label_off' => esc_html__('Hide', 'cafe-lite' ),
            'return_value' => 'true',
            'default' => 'true',
            'condition'     => [
                'layout' => 'carousel',
            ],
        ]);
        $this->add_responsive_control('show_nav', [
            'label'         => esc_html__('Carousel: Navigation', 'cafe-lite'),
            'description'   => esc_html__('', 'cafe-lite'),
            'type'          => Controls_Manager::SWITCHER,
            'label_on' => esc_html__('Show', 'cafe-lite' ),
            'label_off' => esc_html__('Hide', 'cafe-lite' ),
            'return_value' => 'true',
            'default' => 'true',
            'condition'     => [
                'layout' => 'carousel',
            ],
        ]);
        $this->add_control('nav_position', [
            'label'         => esc_html__('Carousel: Navigation position', 'cafe-lite'),
            'description'   => esc_html__('', 'cafe-lite'),
            'type'          => Controls_Manager::SELECT,
            'default'       => 'middle-nav',
            'options' => [
                'top-nav'       => esc_html__('Top', 'cafe-lite' ),
                'middle-nav'    => esc_html__('Middle', 'cafe-lite' ),
            ],
            'condition'     => [
                'show_nav'  => 'true',
                'layout'    => 'carousel',
            ],

        ]);

        $this->end_controls_section();

        $this->start_controls_section('normal_style_settings', [
            'label' => esc_html__('Normal', 'cafe-lite'),
            'tab' => Controls_Manager::TAB_STYLE,
        ]);
        $this->add_control('overlay_banner', [
            'label' => esc_html__('Overlay banner', 'cafe-lite'),
            'type' => Controls_Manager::SWITCHER,
            'description' => esc_html__('Content will show up on image banner.', 'cafe-lite'),
            'return_value' => 'true',
            'default' => 'true',
        ]);
        $this->add_control('effect', [
            'label' => esc_html__('Hover Effect', 'cafe-lite'),
            'type' => Controls_Manager::SELECT,
            'default' => 'normal',
            'condition' => [
                'overlay_banner' => 'true'
            ],
            'options' => [
                'normal' => esc_html__('Normal', 'cafe-lite'),
                'lily' => esc_html__('Lily', 'cafe-lite'),
                'layla' => esc_html__('Layla', 'cafe-lite'),
                'sadie' => esc_html__('Sadie', 'cafe-lite'),
                'oscar' => esc_html__('Oscar', 'cafe-lite'),
                'chico' => esc_html__('Chico', 'cafe-lite'),
                'ruby' => esc_html__('Ruby', 'cafe-lite'),
                'roxy' => esc_html__('Roxy', 'cafe-lite'),
                'marley' => esc_html__('Marley', 'cafe-lite'),
                'sarah' => esc_html__('Sarah', 'cafe-lite'),
                'milo' => esc_html__('Milo', 'cafe-lite'),
            ]
        ]);
        $this->add_control('content_align', [
            'label' => esc_html__('Vertical Align', 'cafe-lite'),
            'type' => Controls_Manager::SELECT,
            'default' => 'flex-start',
            'options' => [
                'flex-start' => esc_html__('Top', 'cafe-lite'),
                'center' => esc_html__('Middle', 'cafe-lite'),
                'flex-end' => esc_html__('Bottom', 'cafe-lite'),
            ],
            'condition' => [
                'overlay_banner' => 'true'
            ],
            'selectors' => [
                '{{WRAPPER}} .cafe-wrap-content' => 'justify-content: {{VALUE}};'
            ]
        ]);
        $this->add_control('text_align', [
            'label' => esc_html__('Text Align', 'cafe-lite'),
            'type' => Controls_Manager::SELECT,
            'default' => 'left',
            'options' => [
                'left' => esc_html__('Left', 'cafe-lite'),
                'center' => esc_html__('Center', 'cafe-lite'),
                'right' => esc_html__('Right', 'cafe-lite'),
            ],
            'selectors' => [
                '{{WRAPPER}} .cafe-wrap-content' => 'text-align: {{VALUE}};'
            ]
        ]);
        $this->add_responsive_control('dimensions', [
            'label' => esc_html__('Dimensions', 'cafe-lite'),
            'type' => Controls_Manager::DIMENSIONS,
            'size_units' => ['px', '%', 'em'],
            'selectors' => [
                '{{WRAPPER}} .cafe-wrap-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
            ],
        ]);
        $this->add_control(
            'title_color_divider',
            [
                'type' => Controls_Manager::DIVIDER,
                'style' => 'thick',
            ]
        );
        $this->add_control('title_color_heading', [
            'label' => esc_html__('Title', 'cafe-lite'),
            'type' => Controls_Manager::HEADING
        ]);
        $this->add_control('title_color', [
            'label' => esc_html__('Title Color', 'cafe-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .cafe-banner-title, {{WRAPPER}} .cafe-wrap-content' => 'color: {{VALUE}};'
            ]
        ]);
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'selector' => '{{WRAPPER}} .cafe-banner-title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
            ]
        );

        $this->add_control(
            'des_color_divider',
            [
                'type' => Controls_Manager::DIVIDER,
                'style' => 'thick',
            ]
        );
        $this->add_control('des_color_heading', [
            'label' => esc_html__('Description', 'cafe-lite'),
            'type' => Controls_Manager::HEADING
        ]);
        $this->add_control('des_color', [
            'label' => esc_html__('Description Color', 'cafe-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .cafe-banner .cafe-banner-description' => 'color: {{VALUE}};'
            ]
        ]);
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'des_typography',
                'selector' => '{{WRAPPER}} .cafe-banner .cafe-banner-description',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
            ]
        );

        $this->add_control(
            'button_style_divider',
            [
                'type' => Controls_Manager::DIVIDER,
                'style' => 'thick',
            ]
        );
        $this->add_control('button_style_heading', [
            'label' => esc_html__('Button', 'cafe-lite'),
            'type' => Controls_Manager::HEADING
        ]);
        $this->add_control('button_color', [
            'label' => esc_html__('Button Color', 'cafe-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .cafe-banner .cafe-button' => 'color: {{VALUE}};'
            ]
        ]);
        $this->add_control('button_bg', [
            'label' => esc_html__('Button Background', 'cafe-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .cafe-banner .cafe-button.outline::after,{{WRAPPER}}  .cafe-banner .cafe-button.normal::after' => 'background: {{VALUE}};'
            ]
        ]);
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'button_typography',
                'selector' => '{{WRAPPER}} .cafe-banner .cafe-button',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
            ]
        );

        $this->add_control('overlay_bg_heading', [
            'label' => esc_html__('Background Overlay', 'cafe-lite'),
            'type' => Controls_Manager::HEADING
        ]);
        $this->add_control(
            'overlay_bg_divider',
            [
                'type' => Controls_Manager::DIVIDER,
                'style' => 'thick',
            ]
        );
        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'overlay_bg',
                'label' => esc_html__('Background Overlay', 'cafe-lite'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .cafe-wrap-content',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section('hover_style_settings', [
            'label' => esc_html__('Hover', 'cafe-lite'),
            'tab' => Controls_Manager::TAB_STYLE,
        ]);
        $this->add_control('title_color_hover', [
            'label' => esc_html__('Title Color', 'cafe-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .cafe-banner:hover .cafe-wrap-content,{{WRAPPER}} .cafe-banner.cafe-overlay-content:hover .cafe-banner-title, {{WRAPPER}} .cafe-banner:not(.cafe-overlay-content) .cafe-banner-title:hover' => 'color: {{VALUE}};'
            ]
        ]);
        $this->add_control('des_color_hover', [
            'label' => esc_html__('Description Color', 'cafe-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .cafe-banner.cafe-overlay-content:hover .cafe-banner-description,{{WRAPPER}} .cafe-banner:not(.cafe-overlay-content) .cafe-banner-description:hover' => 'color: {{VALUE}};'
            ]
        ]);
        $this->add_control('button_color_hover', [
            'label' => esc_html__('Button Color', 'cafe-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .cafe-banner .cafe-button:hover' => 'color: {{VALUE}};'
            ]
        ]);
        $this->add_control('button_bg_hover', [
            'label' => esc_html__('Button Background', 'cafe-lite'),
            'type' => Controls_Manager::COLOR,
            'selectors' => [
                '{{WRAPPER}} .cafe-banner .cafe-button.outline:hover:after,{{WRAPPER}}  .cafe-banner .cafe-button.normal:hover:after' => 'background: {{VALUE}};'
            ]
        ]);
        $this->add_control(
            'overlay_bg_hover_divider',
            [
                'type' => Controls_Manager::DIVIDER,
                'style' => 'thick',
            ]
        );
        $this->add_control('overlay_bg_hover_heading', [
            'label' => esc_html__('Background Overlay', 'cafe-lite'),
            'type' => Controls_Manager::HEADING
        ]);
        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'overlay_bg_hover',
                'label' => esc_html__('Background Overlay', 'cafe-lite'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .cafe-banner.cafe-overlay-content:hover .cafe-wrap-content',
            ]
        );
        $this->end_controls_section();
    }
    /**
     * Load style
     */
    public function get_style_depends()
    {
        return ['cafe-style'];
    }

    /**
     * Retrieve the list of scripts the image carousel widget depended on.
     *
     * Used to set scripts dependencies required to run the widget.
     *
     * @since 1.3.0
     * @access public
     *
     * @return array Widget scripts dependencies.
     */
    public function get_script_depends()
    {
        return ['jquery-slick', 'cafe-script'];
    }
    /**
     * Render
     */
    protected function render()
    {
        $settings = array_merge([ // default settings
            'image' => CAFE_URI . 'assets/img/banner-placeholder.png',
            'auto_size' => 'true',
            'link' => '',
            'css_class' => '',
            'title' => '',
            'title_tag' => 'h3',
            'des' => '',
            'button_label' => '',
            'button_icon' => '',
            'button_icon_pos' => 'after',
            'button_style' => 'normal',
            'overlay_banner' => 'true',
            'effect' => 'normal',

            'columns'               => '',
            'slides_to_show'        => 4,
            'speed'                 => 5000,
            'scroll'                => 1,
            'autoplay'              => 'true',
            'show_pag'              => 'true',
            'show_nav'              => 'true',
            'nav_position'          => 'middle-nav',

        ], $this->get_settings_for_display());

        $this->add_inline_editing_attributes('title');
        $this->add_inline_editing_attributes('description');
        $this->add_inline_editing_attributes('button_label');

        $this->add_render_attribute('title', 'class', 'cafe-banner-title');
        $this->add_render_attribute('des', 'class', 'cafe-banner-description');
        $button_class = 'cafe-button ' . $settings['button_style'];
        $this->add_render_attribute('button_label', 'class', $button_class);

        $this->getViewTemplate('template', 'multi-banner', $settings);
    }
}
