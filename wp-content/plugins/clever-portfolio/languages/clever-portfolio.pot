#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Ri Portfolio I18n\n"
"POT-Creation-Date: 2016-10-21 16:32+0700\n"
"PO-Revision-Date: 2016-10-21 09:21+0700\n"
"Last-Translator: \n"
"Language-Team: CleverSoft WordPress Team <hello@cleversoft.co>\n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: __;_e;_x;esc_html__;esc_attr__;_n\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: assets\n"
"X-Poedit-SearchPathExcluded-1: node_modules\n"
"X-Poedit-SearchPathExcluded-2: languages\n"

#: includes/class-clever-portfolio-category-taxonomy.php:38
#: includes/class-clever-portfolio-shortcode-options-metabox.php:123
msgid "Categories"
msgstr ""

#: includes/class-clever-portfolio-category-taxonomy.php:39
msgid "Category"
msgstr ""

#: includes/class-clever-portfolio-import-export-page.php:42
#: includes/class-clever-portfolio-import-export-page.php:43
msgid "Import/Export"
msgstr ""

#: includes/class-clever-portfolio-import-export-page.php:56
msgid "Import/Export Portfolio Settings"
msgstr ""

#: includes/class-clever-portfolio-import-export-page.php:60
msgid "Import Settings"
msgstr ""

#: includes/class-clever-portfolio-import-export-page.php:62
msgid ""
"Choose a import file from your computer and click \"Upload and Import\" "
"button."
msgstr ""

#: includes/class-clever-portfolio-import-export-page.php:67
msgid "Upload File"
msgstr ""

#: includes/class-clever-portfolio-import-export-page.php:70
msgid "Upload and Import"
msgstr ""

#: includes/class-clever-portfolio-import-export-page.php:77
msgid "Export Settings"
msgstr ""

#: includes/class-clever-portfolio-import-export-page.php:79
msgid ""
"Once you have saved the export file, you can use the import function site to "
"import the settings."
msgstr ""

#: includes/class-clever-portfolio-import-export-page.php:82
msgid "Download Export File"
msgstr ""

#: includes/class-clever-portfolio-import-export-page.php:171
msgid "Settings have been imported successfully!"
msgstr ""

#: includes/class-clever-portfolio-import-export-page.php:175
#: includes/class-clever-portfolio-import-export-page.php:187
#: includes/class-clever-portfolio-settings-page.php:552
#: includes/class-clever-portfolio-settings-page.php:564
msgid "Dismiss this notice."
msgstr ""

#: includes/class-clever-portfolio-import-export-page.php:183
msgid "Failed to import settings. Please try again!"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:70
msgid "Porfolio Options"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:95
msgid "Default Options"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:99
#: includes/class-clever-portfolio-settings-page.php:181
msgid "Extra Information"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:109
msgid "Description"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:114
msgid "A short description which will be displayed on the portfolio page."
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:120
msgid "Format"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:125
#: includes/class-clever-portfolio-options-metabox.php:178
msgid "Gallery"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:128
msgid "Video/Audio"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:136
msgid "Video/Audio URL"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:143
msgid "Preview"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:148
msgid "Failed to get embed resource."
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:157
msgid "Video/Audio Layout"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:162
#: includes/class-clever-portfolio-options-metabox.php:207
msgid "Inherit"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:165
#: includes/class-clever-portfolio-settings-page.php:140
#: includes/class-clever-portfolio-settings-page.php:388
#: includes/class-clever-portfolio-shortcode-options-metabox.php:230
msgid "Standard"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:168
msgid "Full width"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:171
#: includes/class-clever-portfolio-settings-page.php:146
msgid "Background Video"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:192
msgid "Add Gallery"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:195
msgid "Remove Gallery"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:202
msgid "Gallery Layout"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:210
#: includes/class-clever-portfolio-settings-page.php:159
msgid "List"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:213
#: includes/class-clever-portfolio-settings-page.php:162
msgid "Slider"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:216
#: includes/class-clever-portfolio-settings-page.php:165
msgid "Metro"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:219
#: includes/class-clever-portfolio-settings-page.php:168
msgid "Left Sidebar"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:222
#: includes/class-clever-portfolio-settings-page.php:171
msgid "Right Sidebar"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:225
#: includes/class-clever-portfolio-settings-page.php:174
msgid "Full Screen Slider"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:232
msgid "Columns Width"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:237
msgid "The width of each column in the Metro layout."
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:256 clever-portfolio.php:60
msgid "Client Name"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:267 clever-portfolio.php:64
msgid "Date Completion"
msgstr ""

#: includes/class-clever-portfolio-options-metabox.php:344
msgid "Failed to fetch embed resource."
msgstr ""

#: includes/class-clever-portfolio-pagination.php:57 clever-portfolio.php:89
msgid "Load More"
msgstr ""

#: includes/class-clever-portfolio-pagination.php:61
msgid "No More Portfolios"
msgstr ""

#: includes/class-clever-portfolio-post-type.php:41
msgid "RI Portfolios"
msgstr ""

#: includes/class-clever-portfolio-post-type.php:42
msgid "RI Portfolio"
msgstr ""

#: includes/class-clever-portfolio-post-type.php:43
#: includes/class-clever-portfolio-shortcode-post-type.php:43
msgid "Add New"
msgstr ""

#: includes/class-clever-portfolio-post-type.php:44
msgid "Add New Portfolio Item"
msgstr ""

#: includes/class-clever-portfolio-post-type.php:45
msgid "Edit Portfolio Item"
msgstr ""

#: includes/class-clever-portfolio-post-type.php:46
msgid "New Portfolio Item"
msgstr ""

#: includes/class-clever-portfolio-post-type.php:47
msgid "View Portfolio Item"
msgstr ""

#: includes/class-clever-portfolio-post-type.php:48
msgid "Search Portfolio"
msgstr ""

#: includes/class-clever-portfolio-post-type.php:49
msgid "No portfolio items have been added yet"
msgstr ""

#: includes/class-clever-portfolio-post-type.php:50
msgid "Nothing found in Trash"
msgstr ""

#: includes/class-clever-portfolio-post-type.php:84
msgid "Author"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:49
#: includes/class-clever-portfolio-settings-page.php:50
msgid "Settings"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:70
msgid "Portfolio Settings"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:75
#: includes/class-clever-portfolio-shortcode-options-metabox.php:100
msgid "General"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:78
msgid "Single Portfolio"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:81 clever-portfolio.php:68
msgid "Archive Portfolios"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:93
msgid "Post Type Slug"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:97
msgid "The slug in the single portfolio permalink."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:102
msgid "Category Slug"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:106
msgid "The slug in the portfolio archive page permalink."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:111
msgid "Archive Page Title"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:115
msgid "The title for all portfolio archive pages."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:127
msgid "Enable lightbox"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:130
msgid "Whether to use lightbox for each portfolio or not."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:135
msgid "Single Video Layout"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:143
msgid "Full Screen"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:149
msgid "The layout displayed as default for all single video portfolio."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:154
msgid "Single Gallery Layout"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:177
msgid "The layout displayed as default for all single gallery portfolio."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:184
msgid "Whether to use extra information tab on each portfolio or not."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:189
msgid "Available Extra Fields"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:196
msgid ""
"These fields used as extra meta fields on the portfolio options metabox."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:204 clever-portfolio.php:175
msgid "Text"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:205 clever-portfolio.php:176
msgid "Link"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:206 clever-portfolio.php:177
msgid "Email"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:207 clever-portfolio.php:178
msgid "Number"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:208 clever-portfolio.php:179
msgid "Textarea"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:209 clever-portfolio.php:180
msgid "Datetime"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:214 clever-portfolio.php:181
msgid "Remove"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:220
msgid "No extra fields are available to use."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:225
msgid "Add field"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:236
msgid "Generic Settings"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:238
#: includes/class-clever-portfolio-shortcode-options-metabox.php:175
msgid "Order"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:242
#: includes/class-clever-portfolio-shortcode-options-metabox.php:180
msgid "Ascending"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:245
#: includes/class-clever-portfolio-shortcode-options-metabox.php:183
msgid "Descending"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:249
msgid "The order in which portfolios will be displayed."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:254
msgid "Order By"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:258
#: includes/class-clever-portfolio-shortcode-options-metabox.php:198
msgid "Date"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:261
#: includes/class-clever-portfolio-shortcode-options-metabox.php:112
#: includes/class-clever-portfolio-shortcode-options-metabox.php:201
msgid "Title"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:264
#: includes/class-clever-portfolio-shortcode-options-metabox.php:207
msgid "Comment count"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:270
msgid "Display Layout"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:282
#: includes/class-clever-portfolio-shortcode-options-metabox.php:261
msgid "Layout Mode"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:287
#: includes/class-clever-portfolio-shortcode-options-metabox.php:267
msgid "Horiz"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:290
#: includes/class-clever-portfolio-shortcode-options-metabox.php:270
msgid "Masonry"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:293
#: includes/class-clever-portfolio-shortcode-options-metabox.php:273
msgid "Packery"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:296
#: includes/class-clever-portfolio-shortcode-options-metabox.php:276
msgid "Vertical"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:299
#: includes/class-clever-portfolio-shortcode-options-metabox.php:279
msgid "Fit Rows"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:302
#: includes/class-clever-portfolio-shortcode-options-metabox.php:282
msgid "Fit Columns"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:305
#: includes/class-clever-portfolio-shortcode-options-metabox.php:285
msgid "Cells By Row "
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:308
#: includes/class-clever-portfolio-shortcode-options-metabox.php:288
msgid "Cell By Columns"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:311
#: includes/class-clever-portfolio-shortcode-options-metabox.php:291
msgid "Horizontal Masonry"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:315
#: includes/class-clever-portfolio-shortcode-options-metabox.php:295
#, php-format
msgid "The %s in which portfolios will be displayed."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:315
#: includes/class-clever-portfolio-shortcode-options-metabox.php:295
msgid "layout mode"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:320
msgid "Columns per Page"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:324
msgid "The number of columns displayed on an archive page."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:329
msgid "Thumbnail Size"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:351
#: includes/class-clever-portfolio-shortcode-options-metabox.php:322
msgid "Thumbnail size of the portfolios displayed on archive page."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:356
msgid "Portfolio Padding"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:360
msgid "The padding of each portfolio item on the archive page."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:365
msgid "Show Description"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:368
msgid "Whether to show description for each portfolio or not."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:373
msgid "Pagination Settings"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:375
msgid "Portfolios per Page"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:379
msgid "The number of portfolios displayed on an archive page."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:384
msgid "Pagination Type"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:391
#: includes/class-clever-portfolio-shortcode-options-metabox.php:233
msgid "Infinite Scroll"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:394
#: includes/class-clever-portfolio-shortcode-options-metabox.php:236
msgid "Load More Button"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:400
msgid "Infinite Scroll Buffer"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:403
msgid ""
"The higher buffer, the earlier portfolios will be loaded during scrolling."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:407
msgid "Load More Button Text"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:413
msgid "AJAX Loader"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:417
msgid "Upload New"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:420
msgid "Use Default"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:423
msgid "The loading image displayed when stuck in loading portfolios."
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:428
msgid "Custom Settings"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:430
msgid "Enable Custom Style"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:436
msgid "Enable Box Shadow"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:442
msgid "Text Color"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:448
msgid "Hover Text Color"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:454
msgid "Button Text Color"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:460
msgid "Hover Button Text Color"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:466
msgid "Button Background Color"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:472
msgid "Hover Button Background Color"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:478
msgid "Background color"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:484
msgid "Background mask"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:548
msgid "Settings have been saved successfully!"
msgstr ""

#: includes/class-clever-portfolio-settings-page.php:560
msgid "Failed to save settings. Please try again!"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:62
msgid "View more"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:79
msgid "Shortcode Options"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:103
msgid "Layout"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:117
msgid "A title which display right above shortcode content."
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:141
msgid "You can choose multiple categories."
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:147
msgid "Portfolios"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:169
msgid "You can choose multiple portfolios as well."
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:190
msgid "Order by"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:195
msgid "ID"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:204
msgid "Featured"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:210
msgid "Random"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:217
msgid "Number of Portfolios"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:225
msgid "Paging Type"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:239
#: includes/class-clever-portfolio-shortcode-options-metabox.php:397
msgid "None"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:250
msgid "Default Layout"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:300
msgid "Image Size"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:327
msgid "Columns"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:340
msgid "Gutter"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:347
msgid "Hover Effect"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:357
msgid "Filter Align"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:362
msgid "Left"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:365
msgid "Right"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:368
msgid "Center"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:374
msgid "Show Categories"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:380
msgid "Show Read More Button"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:386
msgid "Read More Button Text"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:392
msgid "Animation Type"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:399
msgid "Attention Seekers"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:401
msgid "Flash"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:404
msgid "Bounce"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:407
msgid "Pulse"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:410
msgid "Rubber Band"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:413
msgid "Shake"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:416
msgid "Swing"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:419
msgid "Tada"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:422
msgid "Wobble"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:425
msgid "Bouncing Entrances"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:427
msgid "Bounce In"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:430
msgid "Bounce In Left"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:433
msgid "Bounce In Right"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:436
msgid "Bounce In Up"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:439
msgid "Bounce In Down"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:442
msgid "Fading Entrances"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:444
msgid "Fade In"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:447
msgid "Fade Down"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:450
msgid "Fade Down Big"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:453
msgid "Fade Left"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:456
msgid "Fade Left Big"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:459
msgid "Fade Right"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:462
msgid "Fade Right Big"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:465
msgid "Fade Up"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:468
msgid "Fade Up Big"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:471
msgid "Flippers"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:473
msgid "Flip In"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:476
msgid "Flip In X"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:479
msgid "Flip In Y"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:482
msgid "Lightspeed"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:484
msgid "Light Speed In"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:487
msgid "Roting Entrances"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:489
msgid "Rotate In"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:492
msgid "Rotate In Down Left"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:495
msgid "Rotate In Down Right"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:498
msgid "Rotate In Up Left"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:501
msgid "Rotate In Up Right"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:504
msgid "Slidders"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:506
msgid "Slide In Left"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:509
msgid "Slide In Right"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:512
msgid "Slide In Down"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:515
msgid "Specials"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:517
msgid "Hinge"
msgstr ""

#: includes/class-clever-portfolio-shortcode-options-metabox.php:520
msgid "Roll In"
msgstr ""

#: includes/class-clever-portfolio-shortcode-post-type.php:41
msgid "Shortcodes"
msgstr ""

#: includes/class-clever-portfolio-shortcode-post-type.php:42
#: includes/class-clever-portfolio-shortcode-post-type.php:75
msgid "Shortcode"
msgstr ""

#: includes/class-clever-portfolio-shortcode-post-type.php:44
msgid "Add New Shortcode"
msgstr ""

#: includes/class-clever-portfolio-shortcode-post-type.php:45
msgid "Edit Shortcode"
msgstr ""

#: includes/class-clever-portfolio-shortcode-post-type.php:46
msgid "New Shortcode"
msgstr ""

#: includes/class-clever-portfolio-shortcode-post-type.php:47
msgid "View Shortcode"
msgstr ""

#: includes/class-clever-portfolio-shortcode.php:47
msgid "Portfolio Shortcode"
msgstr ""

#: includes/class-clever-portfolio-shortcode.php:49
msgid "RI Shortcodes"
msgstr ""

#: includes/class-clever-portfolio-shortcode.php:55
msgid "Select a Portfolio Shortcode"
msgstr ""

#: includes/class-clever-portfolio-shortcode.php:56
msgid "Choose a shortcode which you want to insert into this page."
msgstr ""

#: clever-portfolio.php:167 clever-portfolio.php:182
msgid "Select or Upload"
msgstr ""

#: clever-portfolio.php:168
msgid "Add to Gallery"
msgstr ""

#: clever-portfolio.php:173
msgid "Type"
msgstr ""

#: clever-portfolio.php:174
msgid "Label for this field"
msgstr ""

#: clever-portfolio.php:183
msgid "Use this image"
msgstr ""

#: templates/shortcode/carousel.php:77 templates/shortcode/masonry.php:94
msgid "All"
msgstr ""
