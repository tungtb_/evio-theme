<?php
/**
 * The template for displaying Archive portfolio No Content.
 *
 * @package      clever-portfolio\Templates
 * @version      1.0.0
 * @author       Zootemplate
 * @link         http://www.zootemplate.com
 * @copyright    Copyright (c) 2016 Zootemplate
 * @license      GPL v2
 * @since        clever-portfolio 1.0
 */
?>
<h1 class="clever-no-content">
    <?php echo esc_html__('Nothing Found','clever-portfolio')?>
    </h1>
