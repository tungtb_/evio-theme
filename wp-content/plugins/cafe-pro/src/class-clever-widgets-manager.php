<?php namespace Cafe;

use DirectoryIterator;
use Elementor\Repeater;
use Elementor\Widget_Base;
use Elementor\Controls_Stack;
use Elementor\Widgets_Manager;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;

/**
 * CleverWidgetsManager
 *
 * @author CleverSoft <hello.cleversoft@gmail.com>
 * @package CAFE
 */
final class WidgetsManagerPro
{
    /**
     * Plugin settings
     */
    private $settings;

    /**
     * Nope constructor
     */
    private function __construct()
    {
        $this->settings = get_option('cafe_plugin_settings') ? : [];
    }

    /**
     * Singleton
     */
    static function instance($return = false)
    {
        static $self = null;

        if (null === $self) {
            $self = new self;
            add_action('elementor/widgets/widgets_registered', [$self, '_registerWidgets']);
            add_action('elementor/element/after_section_end', [$self, '_addCustomMediaQueries'], PHP_INT_MAX, 3);
            add_action('elementor/widget/before_render_content', [$self, '_renderCustomMediaQueriesForContent'], PHP_INT_MAX);
            add_action('elementor/frontend/column/before_render', [$self, '_renderCustomMediaQueriesForColumn'], PHP_INT_MAX);
            add_action('elementor/frontend/section/before_render', [$self, '_renderCustomMediaQueriesForSection'], PHP_INT_MAX);
        }

        if ($return) {
            return $self;
        }
    }

    /**
     * Register widgets
     *
     * @internal Used as a callback.
     */
    function _registerWidgets(Widgets_Manager $widget_manager)
    {
        $files = new DirectoryIterator(CAFE_PRO_DIR.'src/widgets');

        foreach ($files as $file) {
            if ($file->isFile()) {
                $filename = $file->getFileName();
                if (false !== strpos($filename, '.php') && 'class-clever-widget-base.php' !== $filename) {
                    if (isset($this->settings[$filename]) && '1' === $this->settings[$filename]) {
                        continue;
                    }
                    require CAFE_PRO_DIR.'src/widgets/'.$filename;
                    $classname = $this->getWidgetClassName($filename);
                    $widget = class_exists($classname) ? new $classname() : false;
                    if ($widget && $widget instanceof Widget_Base) {
                        $widget_manager->register_widget_type($widget);
                    }
                }
            }
        }
    }

    /**
     * Add more controls to the built-in widgets of Elementor
     *
     * @internal Used as a callback.
     */
    function _addCustomMediaQueries($widget, $section, $args)
    {
        if ('_section_responsive' === $section) {
    		$widget->start_controls_section(
    			'cafe_custom_media_queries',
    			[
    				'label' => __('Custom Media Queries', 'cafe-pro'),
    				'tab' => Controls_Manager::TAB_ADVANCED,
    			]
    		);

            $repeater = new Repeater();

            $repeater->add_control(
                'enable',
                [
                    'label' => __( 'Enable', 'cafe-pro' ),
                    'type' => Controls_Manager::SWITCHER,
                    'default' => ''
                ]
            );

    		$repeater->add_control(
    			'min_width',
    			[
    				'label' => __( 'Min Width', 'cafe-pro' ),
    				'type' => Controls_Manager::SLIDER,
    				'size_units' => [ 'px' ],
    				'range' => [
    					'px' => [
    						'min' => 0,
    						'max' => 1024
    					]
    				],
    				'default' => [
    					'unit' => 'px',
    					'size' => 767,
    				],
    				'condition' => [
    					'enable!' => '',
    				]
    			]
    		);

    		$repeater->add_control(
    			'max_width',
    			[
    				'label' => __( 'Max Width', 'cafe-pro' ),
    				'type' => Controls_Manager::SLIDER,
    				'size_units' => [ 'px' ],
    				'range' => [
    					'px' => [
    						'min' => 0,
    						'max' => 1920
    					]
    				],
    				'default' => [
    					'unit' => 'px',
    					'size' => 1024,
    				],
    				'condition' => [
    					'enable!' => '',
    				]
    			]
    		);

    		$repeater->add_control(
    			'hidden',
    			[
    				'label' => __( 'Hide', 'cafe-pro' ),
    				'type' => Controls_Manager::SWITCHER,
    				'default' => '',
    				'condition' => [
    					'enable!' => '',
    				],
    			]
    		);

    		$repeater->add_control(
    			'width',
    			[
    				'label' => __( 'Width', 'cafe-pro' ),
    				'type' => Controls_Manager::SLIDER,
    				'size_units' => [ '%', 'px' ],
    				'range' => [
    					'%' => [
    						'min' => 10,
    						'max' => 100
    					],
    					'px' => [
    						'min' => 768,
    						'max' => 1920
    					]
    				],
    				'default' => [
    					'unit' => '%',
    					'size' => 100,
    				],
    				'condition' => [
    					'enable!' => '',
    				],
    			]
    		);

    		$repeater->add_control(
    			'font_size',
    			[
    				'label' => __( 'Font Size', 'cafe-pro' ),
    				'type' => Controls_Manager::SLIDER,
    				'size_units' => [ 'px', 'em', 'rem' ],
    				'range' => [
    					'em' => [
    						'min' => 0.5,
    						'max' => 5,
                            'step' => 0.1
    					],
    					'rem' => [
    						'min' => 0.5,
    						'max' => 5,
                            'step' => 0.1
    					],
    					'px' => [
    						'min' => 10,
    						'max' => 64
    					]
    				],
    				'default' => [
    					'unit' => 'px',
    					'size' => 16,
    				],
    				'condition' => [
    					'enable!' => '',
    				],
    			]
    		);

    		$repeater->add_control(
    			'line_height',
    			[
    				'label' => __( 'Line Height', 'cafe-pro' ),
    				'type' => Controls_Manager::SLIDER,
    				'size_units' => [ 'px' ],
    				'range' => [
    					'px' => [
    						'min' => 10,
    						'max' => 64
    					]
    				],
    				'default' => [
    					'unit' => 'px',
    					'size' => 16,
    				],
    				'condition' => [
    					'enable!' => '',
    				],
    			]
    		);

    		$repeater->add_control(
    			'margin',
    			[
    				'label' => __( 'Margin', 'cafe-pro' ),
    				'type' => Controls_Manager::DIMENSIONS,
    				'size_units' => [ 'px', '%' ],
    				'range' => [
    					'px' => [
    						'min' => 0,
    						'max' => 100
    					],
    					'%' => [
    						'min' => 0,
    						'max' => 100
    					]
    				],
    				'condition' => [
    					'enable!' => '',
    				],
    			]
    		);

    		$repeater->add_control(
    			'padding',
    			[
    				'label' => __( 'Padding', 'cafe-pro' ),
    				'type' => Controls_Manager::DIMENSIONS,
    				'size_units' => [ 'px', '%' ],
    				'range' => [
    					'px' => [
    						'min' => 0,
    						'max' => 100
    					],
    					'%' => [
    						'min' => 0,
    						'max' => 100
    					]
    				],
    				'condition' => [
    					'enable!' => '',
    				],
    			]
    		);

    		$repeater->add_control(
    			'align',
    			[
    				'label' => __( 'Alignment', 'cafe-pro' ),
    				'type' => Controls_Manager::CHOOSE,
    				'options' => [
    					'left' => [
    						'title' => __( 'Left', 'cafe-pro' ),
    						'icon' => 'fa fa-align-left',
    					],
    					'center' => [
    						'title' => __( 'Center', 'cafe-pro' ),
    						'icon' => 'fa fa-align-center',
    					],
    					'right' => [
    						'title' => __( 'Right', 'cafe-pro' ),
    						'icon' => 'fa fa-align-right',
    					],
    				],
    				'condition' => [
    					'enable!' => '',
    				],
    			]
    		);

    		$widget->add_control(
    			'cafe_custom_queries_items',
    			[
    				'type' => Controls_Manager::REPEATER,
    				'fields' => $repeater->get_controls(),
    				'title_field' => '{{{ min_width.size }}}px - {{{ max_width.size }}}px',
    			]
    		);

    		$widget->end_controls_section();
        }
    }

    /**
     * Render custom media queries
     *
     * @internal Used as a callback
     */
    function _renderCustomMediaQueriesForSection($section)
    {
        $settings = $section->get_settings();

        if (!empty($settings['cafe_custom_queries_items'])) {
            $_id = $section->get_id();
            $style = '';
            foreach ($settings['cafe_custom_queries_items'] as $item) {
                if ('yes' !== $item['enable']) continue;
                $style .= '@media ';
				if (!empty($item['min_width']['size'])) {
					$style .= '(min-width:'. intval($item['min_width']['size']) .'px)';
				}
				if (!empty($item['min_width']['size']) && !empty($item['max_width']['size'])) {
					$style .= ' and ';
				}
				if (!empty($item['max_width']['size'])) {
					$style .= '(max-width:'. intval($item['max_width']['size']) . 'px)';
				}
                $style .= '{.elementor-element.elementor-element-' . $_id . '{';
				if ('yes' === $item['hidden']) {
					$style .= 'display:none;';
				}
                if (!empty($item['width']['size'])) {
                    $style .= 'width:'. intval($item['width']['size']) . $item['width']['unit'] . ';';
                }
                if (!empty($item['font_size']['size'])) {
                    $style .= 'font-size:'. intval($item['font_size']['size']) . $item['font_size']['unit'] . ';';
                }
                if (!empty($item['line_height']['size'])) {
                    $style .= 'line-height:'. intval($item['line_height']['size']) . $item['line_height']['unit'] . ';';
                }
                if (is_numeric($item['margin']['top'])) {
                    $style .= 'margin-top:'. intval($item['margin']['top']) . $item['margin']['unit'] . ';';
                }
                if (is_numeric($item['margin']['right'])) {
                    $style .= 'margin-right:'. intval($item['margin']['right']) . $item['margin']['unit'] . ';';
                }
                if (is_numeric($item['margin']['bottom'])) {
                    $style .= 'margin-bottom:'. intval($item['margin']['bottom']) . $item['margin']['unit'] . ';';
                }
                if (is_numeric($item['margin']['left'])) {
                    $style .= 'margin-left:'. intval($item['margin']['left']) . $item['margin']['unit'] . ';';
                }
                if (is_numeric($item['padding']['top'])) {
                    $style .= 'padding-top:'. intval($item['padding']['top']) . $item['padding']['unit'] . ';';
                }
                if (is_numeric($item['padding']['right'])) {
                    $style .= 'padding-right:'. intval($item['padding']['right']) . $item['padding']['unit'] . ';';
                }
                if (is_numeric($item['padding']['bottom'])) {
                    $style .= 'padding-bottom:'. intval($item['padding']['bottom']) . $item['padding']['unit'] . ';';
                }
                if (is_numeric($item['padding']['left'])) {
                    $style .= 'padding-left:'. intval($item['padding']['left']) . $item['padding']['unit'] . ';';
                }
                if (!empty($item['align'])) {
                    $style .= 'text-align:'. $item['align'] . ';';
                }
                $style .= '}}';
            }
            add_action('wp_footer', function() use ($style)
            {
                echo '<style>' . $style . '</style>';
            }, 10, 0);
        }
    }

    /**
     * Render custom media queries
     *
     * @internal Used as a callback
     */
    function _renderCustomMediaQueriesForColumn($col)
    {
        $settings = $col->get_settings();

        if (!empty($settings['cafe_custom_queries_items'])) {
            $_id = $col->get_id();
            $style = '';
            foreach ($settings['cafe_custom_queries_items'] as $item) {
                if ('yes' !== $item['enable']) continue;
                $style .= '@media ';
				if (!empty($item['min_width']['size'])) {
					$style .= '(min-width:'. intval($item['min_width']['size']) .'px)';
				}
				if (!empty($item['min_width']['size']) && !empty($item['max_width']['size'])) {
					$style .= ' and ';
				}
				if (!empty($item['max_width']['size'])) {
					$style .= '(max-width:'. intval($item['max_width']['size']) . 'px)';
				}
                $style .= '{.elementor-element.elementor-section .elementor-element.elementor-element-' . $_id . '>.elementor-element-populated{';
				if ('yes' === $item['hidden']) {
					$style .= 'display:none;';
				}
                if (!empty($item['width']['size'])) {
                    $style .= 'width:'. intval($item['width']['size']) . $item['width']['unit'] . ';';
                }
                if (!empty($item['font_size']['size'])) {
                    $style .= 'font-size:'. intval($item['font_size']['size']) . $item['font_size']['unit'] . ';';
                }
                if (!empty($item['line_height']['size'])) {
                    $style .= 'line-height:'. intval($item['line_height']['size']) . $item['line_height']['unit'] . ';';
                }
                if (is_numeric($item['margin']['top'])) {
                    $style .= 'margin-top:'. intval($item['margin']['top']) . $item['margin']['unit'] . ';';
                }
                if (is_numeric($item['margin']['right'])) {
                    $style .= 'margin-right:'. intval($item['margin']['right']) . $item['margin']['unit'] . ';';
                }
                if (is_numeric($item['margin']['bottom'])) {
                    $style .= 'margin-bottom:'. intval($item['margin']['bottom']) . $item['margin']['unit'] . ';';
                }
                if (is_numeric($item['margin']['left'])) {
                    $style .= 'margin-left:'. intval($item['margin']['left']) . $item['margin']['unit'] . ';';
                }
                if (is_numeric($item['padding']['top'])) {
                    $style .= 'padding-top:'. intval($item['padding']['top']) . $item['padding']['unit'] . ';';
                }
                if (is_numeric($item['padding']['right'])) {
                    $style .= 'padding-right:'. intval($item['padding']['right']) . $item['padding']['unit'] . ';';
                }
                if (is_numeric($item['padding']['bottom'])) {
                    $style .= 'padding-bottom:'. intval($item['padding']['bottom']) . $item['padding']['unit'] . ';';
                }
                if (is_numeric($item['padding']['left'])) {
                    $style .= 'padding-left:'. intval($item['padding']['left']) . $item['padding']['unit'] . ';';
                }
                if (!empty($item['align'])) {
                    $style .= 'text-align:'. $item['align'] . ';';
                }
                $style .= '}}';
            }
            add_action('wp_footer', function() use ($style)
            {
                echo '<style>' . $style . '</style>';
            }, 11, 0);
        }
    }

    /**
     * Render custom media queries
     *
     * @internal Used as a callback
     */
    function _renderCustomMediaQueriesForContent($el)
    {
        $settings = $el->get_settings();

        if (!empty($settings['cafe_custom_queries_items'])) {
            $_id = $el->get_id();
            $style = '';
            foreach ($settings['cafe_custom_queries_items'] as $item) {
                if ('yes' !== $item['enable']) continue;
                $style .= '@media ';
				if (!empty($item['min_width']['size'])) {
					$style .= '(min-width:'. intval($item['min_width']['size']) .'px)';
				}
				if (!empty($item['min_width']['size']) && !empty($item['max_width']['size'])) {
					$style .= ' and ';
				}
				if (!empty($item['max_width']['size'])) {
					$style .= '(max-width:'. intval($item['max_width']['size']) . 'px)';
				}
                $style .= '{.elementor-element.elementor-element-' . $_id . '{';
				if ('yes' === $item['hidden']) {
					$style .= 'display:none;';
				}
                if (!empty($item['width']['size'])) {
                    $style .= 'width:'. intval($item['width']['size']) . $item['width']['unit'] . ';';
                }
                if (!empty($item['font_size']['size'])) {
                    $style .= 'font-size:'. intval($item['font_size']['size']) . $item['font_size']['unit'] . ';';
                }
                if (!empty($item['line_height']['size'])) {
                    $style .= 'line-height:'. intval($item['line_height']['size']) . $item['line_height']['unit'] . ';';
                }
                if (is_numeric($item['margin']['top'])) {
                    $style .= 'margin-top:'. intval($item['margin']['top']) . $item['margin']['unit'] . ';';
                }
                if (is_numeric($item['margin']['right'])) {
                    $style .= 'margin-right:'. intval($item['margin']['right']) . $item['margin']['unit'] . ';';
                }
                if (is_numeric($item['margin']['bottom'])) {
                    $style .= 'margin-bottom:'. intval($item['margin']['bottom']) . $item['margin']['unit'] . ';';
                }
                if (is_numeric($item['margin']['left'])) {
                    $style .= 'margin-left:'. intval($item['margin']['left']) . $item['margin']['unit'] . ';';
                }
                if (is_numeric($item['padding']['top'])) {
                    $style .= 'padding-top:'. intval($item['padding']['top']) . $item['padding']['unit'] . ';';
                }
                if (is_numeric($item['padding']['right'])) {
                    $style .= 'padding-right:'. intval($item['padding']['right']) . $item['padding']['unit'] . ';';
                }
                if (is_numeric($item['padding']['bottom'])) {
                    $style .= 'padding-bottom:'. intval($item['padding']['bottom']) . $item['padding']['unit'] . ';';
                }
                if (is_numeric($item['padding']['left'])) {
                    $style .= 'padding-left:'. intval($item['padding']['left']) . $item['padding']['unit'] . ';';
                }
                if (!empty($item['align'])) {
                    $style .= 'text-align:'. $item['align'] . ';';
                }
                $style .= '}}';
            }
            add_action('wp_footer', function() use ($style)
            {
                echo '<style>' . $style . '</style>';
            }, 13, 0);
        }
    }

    /**
     * Get classname from filename
     *
     * @param string $filename
     *
     * @return string
     */
    private function getWidgetClassName($filename)
    {
        $_filename = trim(str_replace(['class', '-', '.php'], ['', ' ', ''], $filename));

        return sprintf('%s\Widgets\%s', __NAMESPACE__, str_replace(' ', '', ucwords($_filename)));
    }
}

// Initialize.
WidgetsManagerPro::instance();
