<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'evio.zootemplate.com' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'mysql' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#!:2r&lp7^)P>Jt24:M,CiG/vgd%`I:KCNoQFxTmPAgK~gP??m@{I{7SK8TtP-qk' );
define( 'SECURE_AUTH_KEY',  'VAc_t~AdGzJF~?c`~d|B>M;<.$+i@v0CAGnT[(CF|S 2z-b3?^OcT4_K4Y*N98!)' );
define( 'LOGGED_IN_KEY',    'Wz%_7<L_3iN}3rO<?fe$1um,68|9yKvQPNVSXf}YFBR0zX7PuKMWi2YM4SHKp_Us' );
define( 'NONCE_KEY',        'NGP:W(AP/+nu0m6b9essHR0%tZYwSCf~E $m:NUb8`bXOjYDnH~]. ]Z5ItRPJZ1' );
define( 'AUTH_SALT',        '|+#gEy)-7a{HgA.ymf6{;j14TS,k[R72okd~|)%*Fv{nv%cou i,k|,X(h@=-N^M' );
define( 'SECURE_AUTH_SALT', 'sDIZ+Mx$QXld_VO~>v&gUDpYIB2NKfRx(x#?:Dwh.3+2Cnu@`$v`;wIHyL*^cFG~' );
define( 'LOGGED_IN_SALT',   '>Er5po{vJ8RZ=7N/Rax#qX$B!.vsIxvM]7%~+G9Dk<(;Q!0Hl)nd4O)(,WOW]_@p' );
define( 'NONCE_SALT',       'FNT,z8$!T)]0Px2?P6X(!5Y:k;drh6V%KL!;$7YE[~L`|Nc8[+CfuBLT%v-2A_{C' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
